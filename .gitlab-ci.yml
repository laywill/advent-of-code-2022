---
# This file is based from a template, and might need editing before it works on your project.
# To contribute improvements to CI/CD templates, please follow the Development guide at:
# https://docs.gitlab.com/ee/development/cicd/templates.html
# This specific template is located at:
# https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Python.gitlab-ci.yml

# Change pip's cache directory to be inside the project directory since we can
# only cache local items.
variables:
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"
  # Exclude vulnerabilities from output based on the paths.
  # Exclude directories used by build tools as these can generate false positives.
  SAST_EXCLUDED_PATHS: spec, test, tests, tmp, venv
  PRE_COMMIT_HOME: ${CI_PROJECT_DIR}/.cache/pre-commit

.python_cache:
  # Pip's cache doesn't store the python packages
  # https://pip.pypa.io/en/stable/topics/caching/
  #
  # If you want to also cache the installed packages, you have to install
  # them in a virtualenv and cache it as well.
  cache:
    paths:
      - .cache/pip
      - venv/
      - ${PRE_COMMIT_HOME}

# Official language image. Look for the different tagged releases at:
# https://hub.docker.com/r/library/python/tags/
.python:
  image: python:3.11
  tags:
    - docker
  extends:
    - .python_cache
  before_script:
    - python -V # Print out python version for debugging
    - pip install virtualenv
    - virtualenv venv
    - source venv/bin/activate
    - pip install -r requirements.txt

# Define Pipeline stages
stages:
  - lint
  - static_analysis
  - test
  - run
  - document
  - deploy

.lint:
  allow_failure: true
  stage: lint

.static_analysis:
  allow_failure: true
  stage: static_analysis

##################################################
#
# LINT
##################################################

# MegaLinter GitLab CI job configuration file
# More info at https://megalinter.github.io/
mega-linter:
  extends:
    - .lint
  tags:
    - docker
  # You can override MegaLinter flavour used to have faster performances
  # More info at https://megalinter.github.io/flavors/
  image: oxsecurity/megalinter-python:latest
  script: ["/bin/bash /entrypoint.sh"]
  variables:
    # All available variables are described in documentation
    # https://megalinter.github.io/configuration/
    DEFAULT_WORKSPACE: $CI_PROJECT_DIR
    # ADD YOUR CUSTOM ENV VARIABLES HERE TO OVERRIDE VALUES OF .mega-linter.yml AT THE ROOT OF YOUR REPOSITORY
  artifacts:
    when: always
    paths:
      - megalinter-reports
    expire_in: 1 week

pydocstyle:
  extends:
    - .python
    - .lint
  script:
    - pip install pydocstyle
    - pydocstyle ./src/
    # - pydocstyle ./tests/

precommit:
  extends:
    - .python
    - .lint
  script:
    - pre-commit install
    - pre-commit run --all-files --verbose

##################################################
#
# Static Analysis
##################################################

# bandit:
#   extends:
#     - .python
#     - .lint
#   script:
#     - pip install bandit
#     - bandit -s B101 -ll -f screen ./src/ -r
#     - bandit -s B101 -ll -f screen ./tests/ -r

python-safety:
  extends:
    - .python
    - .static_analysis
  image: registry.gitlab.com/pipeline-components/python-safety:latest
  script:
    - safety check --full-report -r requirements.txt

radon_cyclomatic_complexity:
  extends:
    - .python
    - .static_analysis
  script:
    - pip install radon
    - radon cc --show-complexity --total-average ./src/
    # - radon cc --show-complexity --total-average ./tests/

radon_maintainability_indexes:
  extends:
    - .python
    - .static_analysis
  script:
    - pip install radon
    - radon mi --show ./src/
    # - radon mi --show ./tests/

radon_halstead complexity:
  extends:
    - .python
    - .static_analysis
  script:
    - pip install radon
    - radon hal ./src/
    # - radon hal ./tests/

radon_raw_stats:
  extends:
    - .python
    - .static_analysis
  script:
    - pip install radon
    - radon raw --summary ./src/
    # - radon raw --summary ./tests/

##################################################
#
# SAST
##################################################

# You can override the included template(s) by including variable overrides
# SAST customization: https://docs.gitlab.com/ee/user/application_security/sast/#customizing-the-sast-settings
# Secret Detection customization: https://docs.gitlab.com/ee/user/application_security/secret_detection/#customizing-settings
# Dependency Scanning customization: https://docs.gitlab.com/ee/user/application_security/dependency_scanning/#customizing-the-dependency-scanning-settings
# Container Scanning customization: https://docs.gitlab.com/ee/user/application_security/container_scanning/#customizing-the-container-scanning-settings
# Note that environment variables can be set in several places
# See https://docs.gitlab.com/ee/ci/variables/#cicd-variable-precedence
include:
  - template: Security/SAST.gitlab-ci.yml

##################################################
#
# Unit Test
##################################################

# test:
#   script:
#     - python setup.py test
#     - pip install tox flake8  # you can also use tox
#     - tox -e py36,flake8

pytest:
  extends:
    - .python
  stage: test
  script:
    - pip install pytest pytest-cov
    - pytest --cov=src tests/
    - coverage xml
  coverage: '/(?i)total.*? (100(?:\.0+)?\%|[1-9]?\d(?:\.\d+)?\%)$/'
  artifacts:
    reports:
      coverage_report:
        coverage_format: cobertura
        path: coverage.xml

##################################################
#
# RUN
##################################################

run:
  extends:
    - .python
  stage: run
  script:
    - chmod +x ./scripts/run_all_python.sh
    - ./scripts/run_all_python.sh
  artifacts:
    expire_in: 1 week
    paths:
      - output/**/*.txt

##################################################
#
# Document
##################################################

# pages:
#   extends:
#     - .python
#   stage: document
#   script:
#     - pip install sphinx sphinx-rtd-theme
#     - cd doc
#     - make html
#     - mv build/html/ ../public/
#   artifacts:
#     paths:
#       - public
#   rules:
#     - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

##################################################
#
# Deploy
##################################################

# deploy:
#   extends:
#     - .python
#   stage: deploy
#   script: echo "Define your deployment script!"
#   environment: production
