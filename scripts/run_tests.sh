#!/bin/bash

# Safer bash scripts by causing it to fail fast, safely and early.
set -euo pipefail

pytest --cov=src tests/
