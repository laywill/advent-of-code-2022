$files = Get-ChildItem -Path .\src\day*.py -Recurse
# Write-Output $files

foreach($file in $files)
{
    Write-Output ""
    Write-Output "Running $file"
    Measure-Command -Expression {python $file}
}
