#!/bin/bash

# Safer bash scripts by causing it to fail fast, safely and early.
set -euo pipefail

# In this project we will prefer the use of out-of-tree building and execution.
# This allows for cleaner commits, and easier excluding of files and folders from linters.

# Create top level folders
mkdir -p "./src/"
mkdir -p "./tests/"
mkdir -p "./data/"
mkdir -p "./output/"

# Create a location for common code that will be reused, e.g. file reading
mkdir -p "./src/common/"
mkdir -p "./tests/common/"

# Create folders for each of the days
for day in {01..25}; do
	mkdir -p "./src/day_${day}/"
	mkdir -p "./tests/${day}/"
	mkdir -p "./data/${day}/"
	mkdir -p "./output/${day}/"
done
