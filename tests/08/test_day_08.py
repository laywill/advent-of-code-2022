import os
import sys
from pathlib import Path

import numpy as np

current_path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, current_path + '/../../')

# from src.common import aoc_file_io
from src.day_08 import day_08  # noqa: E402

# Day of the AOC
day = 8
data_dir = f"data/{day:02}/"


def _read_input(data_filepath):
    # Define the example data file and the puzzle input data
    INPUT_PATH_TEST = Path.cwd() / f"./{data_filepath}"

    if not INPUT_PATH_TEST.is_file():
        raise FileNotFoundError(
            f"Error: File does not exist: {INPUT_PATH_TEST}"
        )

    with INPUT_PATH_TEST.open(mode="r", encoding="utf-8") as in_file_test:
        input_test_str = in_file_test.read()

    return input_test_str


def test_process_input():
    file_name = f'{data_dir}day_{day:02}_test.txt'
    input_str = _read_input(file_name)
    result = day_08.process_input(input_str)
    expected = np.array(
        [
            [3, 0, 3, 7, 3],
            [2, 5, 5, 1, 2],
            [6, 5, 3, 3, 2],
            [3, 3, 5, 4, 9],
            [3, 5, 3, 9, 0],
        ]
    )

    assert np.array_equal(result, expected)


def test_part1():
    """Can the algorithm find the number of trees visible from the edge of the trees."""
    file_name = f'{data_dir}day_{day:02}_test.txt'
    input_str = _read_input(file_name)
    parsed_data = day_08.process_input(input_str)
    tree_map = day_08.Trees(parsed_data)
    result = day_08.part1(tree_map)

    expected = 21

    assert result == expected


def test_part2():
    """Can the algorithm find the tree with the most trees visible from it, and return it's scenic score."""
    file_name = f'{data_dir}day_{day:02}_test.txt'
    input_str = _read_input(file_name)
    parsed_data = day_08.process_input(input_str)
    tree_map = day_08.Trees(parsed_data)
    result = day_08.part2(tree_map)

    expected = 8

    assert result == expected
