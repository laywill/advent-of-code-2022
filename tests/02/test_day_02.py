import os
import sys
from pathlib import Path

current_path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, current_path + '/../../')

# from src.common import aoc_file_io
from src.day_02 import day_02  # noqa: E402

# Day of the AOC
day = 2
data_dir = f"data/{day:02}/"
file_name_test = f'{data_dir}day_{day:02}_test.txt'

# Define the example data file and the puzzle input data
INPUT_PATH_TEST = Path.cwd() / f"./{file_name_test}"

if not INPUT_PATH_TEST.is_file():
    raise FileNotFoundError(f"Error: File does not exist: {INPUT_PATH_TEST}")


def test_process_input():
    # Read the files and convert to lists of ints
    with INPUT_PATH_TEST.open(mode="r", encoding="utf-8") as in_file_test:
        report_test = in_file_test.read().strip()

    result = day_02.process_input(report_test)
    expected = (("A", "Y"), ("B", "X"), ("C", "Z"))

    assert result == expected


def test_part1():
    # Return total score, decoding my moves as Rock Paper or Scissors.
    with INPUT_PATH_TEST.open(mode="r", encoding="utf-8") as in_file_test:
        report_test = in_file_test.read().strip()

    data = day_02.process_input(report_test)
    result = day_02.part1(data)

    expected = 15

    assert result == expected


def test_part2():
    # Return total score, decoding my hands as match result.
    with INPUT_PATH_TEST.open(mode="r", encoding="utf-8") as in_file_test:
        report_test = in_file_test.read().strip()

    data = day_02.process_input(report_test)
    result = day_02.part2(data)

    expected = 12

    assert result == expected
