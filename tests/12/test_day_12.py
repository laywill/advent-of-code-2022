import os
import sys
from pathlib import Path

import numpy as np

current_path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, current_path + '/../../')

# from src.common import aoc_file_io
from src.day_12 import day_12  # noqa: E402

# Day of the AOC
day = 12
data_dir = f"data/{day:02}/"


def _read_input(data_filepath):
    # Define the example data file and the puzzle input data
    INPUT_PATH_TEST = Path.cwd() / f"./{data_filepath}"

    if not INPUT_PATH_TEST.is_file():
        raise FileNotFoundError(
            f"Error: File does not exist: {INPUT_PATH_TEST}"
        )

    with INPUT_PATH_TEST.open(mode="r", encoding="utf-8") as in_file_test:
        input_test_str = in_file_test.read()

    return input_test_str
    # ip_parsed_data = day_12.process_input(input_test_str)
    # return ip_parsed_data


def test_process_input():
    file_name = f'{data_dir}day_{day:02}_test.txt'
    input_str = _read_input(file_name)
    result = day_12.process_input(input_str)
    expected = np.array(
        [
            ['S', 'a', 'b', 'q', 'p', 'o', 'n', 'm'],
            ['a', 'b', 'c', 'r', 'y', 'x', 'x', 'l'],
            ['a', 'c', 'c', 's', 'z', 'E', 'x', 'k'],
            ['a', 'c', 'c', 't', 'u', 'v', 'w', 'j'],
            ['a', 'b', 'd', 'e', 'f', 'g', 'h', 'i'],
        ]
    )

    assert np.array_equal(result, expected)


def _test_part1_one_row():
    """Can the algorithm successfully climb a row in alphabetical order."""
    file_name = f'{data_dir}day_{day:02}_test_x_climb.txt'
    input_str = _read_input(file_name)
    parsed_data = day_12.process_input(input_str)
    result = day_12.part1(parsed_data)

    expected = 25

    assert result == expected


def _test_part1_one_col():
    """Can the algorithm successfully climb a column in alphabetical order."""
    file_name = f'{data_dir}day_{day:02}_test_y_climb.txt'
    input_str = _read_input(file_name)
    parsed_data = day_12.process_input(input_str)
    result = day_12.part1(parsed_data)

    expected = 25

    assert result == expected


def _test_part1_one_row_with_drop():
    """Can the algorithm successfully climb a row in alphabetical order."""
    file_name = f'{data_dir}day_{day:02}_test_x_climb_with_drop.txt'
    input_str = _read_input(file_name)
    parsed_data = day_12.process_input(input_str)
    result = day_12.part1(parsed_data)

    expected = 29

    assert result == expected


def _test_part1_one_col_with_drop():
    """Can the algorithm successfully climb a column in alphabetical order."""
    file_name = f'{data_dir}day_{day:02}_test_y_climb_with_drop.txt'
    input_str = _read_input(file_name)
    parsed_data = day_12.process_input(input_str)
    result = day_12.part1(parsed_data)

    expected = 29

    assert result == expected


def _test_part1_xy():
    """Can the algorithm successfully climb a snaking XY trail in alphabetical order."""
    file_name = f'{data_dir}day_{day:02}_test_xy_climb.txt'
    input_str = _read_input(file_name)
    parsed_data = day_12.process_input(input_str)
    result = day_12.part1(parsed_data)

    expected = 26

    assert result == expected


def _test_part1_xy_with_drop():
    """Can the algorithm successfully climb a snaking XY trail in alphabetical order with a drop."""
    file_name = f'{data_dir}day_{day:02}_test_xy_climb_with_drop.txt'
    input_str = _read_input(file_name)
    parsed_data = day_12.process_input(input_str)
    result = day_12.part1(parsed_data)

    expected = 32

    assert result == expected


def _test_part1_spiral():
    """Can the algorithm successfully climb a spiral."""
    file_name = f'{data_dir}day_{day:02}_test_spiral.txt'
    input_str = _read_input(file_name)
    parsed_data = day_12.process_input(input_str)
    result = day_12.part1(parsed_data)

    expected = 35

    assert result == expected


def _test_part1():
    """Can the algorithm successfully get the same answer as AOC example."""
    file_name = f'{data_dir}day_{day:02}_test.txt'
    input_str = _read_input(file_name)
    parsed_data = day_12.process_input(input_str)
    result = day_12.part1(parsed_data)

    expected = 31

    assert result == expected


def test_part1():
    _test_part1_one_row()
    _test_part1_one_col()
    _test_part1_one_row_with_drop()
    _test_part1_one_col_with_drop()
    _test_part1_xy_with_drop()
    _test_part1_spiral()
    _test_part1()


def test_part2():
    """Can the algorithm find the shortest number of steps for any valid start."""
    file_name = f'{data_dir}day_{day:02}_test.txt'
    input_str = _read_input(file_name)
    parsed_data = day_12.process_input(input_str)
    result = day_12.part2(parsed_data)

    expected = 29

    assert result == expected
