import os
import sys
from pathlib import Path

current_path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, current_path + '/../../')

# from src.common import aoc_file_io
from src.day_01 import day_01  # noqa: E402

# Day of the AOC
day = 1
data_dir = f"data/{day:02}/"
file_name_test = f'{data_dir}day_{day:02}_test.txt'

# Define the example data file and the puzzle input data
INPUT_PATH_TEST = Path.cwd() / f"./{file_name_test}"

if not INPUT_PATH_TEST.is_file():
    raise FileNotFoundError(f"Error: File does not exist: {INPUT_PATH_TEST}")


def test_process_input():
    # Read the files and convert to lists of ints
    with INPUT_PATH_TEST.open(mode="r", encoding="utf-8") as in_file_test:
        report_test = in_file_test.read().strip()

    result = day_01.process_input(report_test)
    expected = [
        [1000, 2000, 3000],
        [4000],
        [5000, 6000],
        [7000, 8000, 9000],
        [10000],
    ]

    assert result == expected


def test_total_calories_per_elf():
    # Read the files and convert to lists of ints
    with INPUT_PATH_TEST.open(mode="r", encoding="utf-8") as in_file_test:
        report_test = in_file_test.read().strip()

    result = day_01.total_calories_per_elf(day_01.process_input(report_test))
    expected = [6000, 4000, 11000, 24000, 10000]

    assert result == expected


def test_part1():
    # Read the files and convert to lists of ints
    with INPUT_PATH_TEST.open(mode="r", encoding="utf-8") as in_file_test:
        report_test = in_file_test.read().strip()

    result = day_01.part1(
        day_01.total_calories_per_elf(day_01.process_input(report_test))
    )
    expected = 24000

    assert result == expected


def test_part2():
    # Read the files and convert to lists of ints
    with INPUT_PATH_TEST.open(mode="r", encoding="utf-8") as in_file_test:
        report_test = in_file_test.read().strip()

    result = day_01.part2(
        day_01.total_calories_per_elf(day_01.process_input(report_test))
    )
    expected = 45000

    assert result == expected
