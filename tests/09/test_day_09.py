import os
import sys
from pathlib import Path

current_path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, current_path + '/../../')

# from src.common import aoc_file_io
from src.day_09 import day_09  # noqa: E402

# Day of the AOC
day = 9
data_dir = f"data/{day:02}/"


def _read_input(data_filepath):
    # Define the example data file and the puzzle input data
    INPUT_PATH_TEST = Path.cwd() / f"./{data_filepath}"

    if not INPUT_PATH_TEST.is_file():
        raise FileNotFoundError(
            f"Error: File does not exist: {INPUT_PATH_TEST}"
        )

    with INPUT_PATH_TEST.open(mode="r", encoding="utf-8") as in_file_test:
        input_str = in_file_test.read().strip()
    return input_str


def test_rope():
    rope = day_09.Rope(2)

    # Confirm that number of knots is initialised to the parameter passed in
    assert rope.num_knots == 2

    # Confirm origin correctly initialised
    assert rope.origin == (0, 0)

    # Confirm initialised with the origin visited as this is where the tail starts
    assert rope.visited == {(0, 0)}

    assert rope.knots == {0: {"x": 0, "y": 0}, 1: {"x": 0, "y": 0}}


def test_process_input():
    file_name_test = f'{data_dir}day_{day:02}_test_01.txt'
    input_str = _read_input(file_name_test)
    result = day_09.process_input(input_str)

    expected = [
        ["R", 4],
        ["U", 4],
        ["L", 3],
        ["D", 1],
        ["R", 4],
        ["D", 1],
        ["L", 5],
        ["R", 2],
    ]

    assert result == expected


def test_part1():
    file_name_test = f'{data_dir}day_{day:02}_test_01.txt'
    input_str = _read_input(file_name_test)
    parsed_data = day_09.process_input(input_str)
    result = day_09.part1(parsed_data)

    expected = 13

    assert result == expected


def test_part2():
    file_name_test = f'{data_dir}day_{day:02}_test_02.txt'
    input_str = _read_input(file_name_test)
    parsed_data = day_09.process_input(input_str)
    result = day_09.part2(parsed_data)

    expected = 36

    assert result == expected
