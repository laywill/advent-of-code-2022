import os
import sys
from pathlib import Path

import numpy as np

current_path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, current_path + '/../../')

# from src.common import aoc_file_io
from src.day_14 import day_14  # noqa: E402

# Day of the AOC
day = 14
data_dir = f"data/{day:02}/"


def _read_input(data_filepath):
    # Define the example data file and the puzzle input data
    INPUT_PATH_TEST = Path.cwd() / f"./{data_filepath}"

    if not INPUT_PATH_TEST.is_file():
        raise FileNotFoundError(
            f"Error: File does not exist: {INPUT_PATH_TEST}"
        )

    with INPUT_PATH_TEST.open(mode="r", encoding="utf-8") as in_file_test:
        input_test_str = in_file_test.read()

    return input_test_str


def test_part1():
    file_name = f'{data_dir}day_{day:02}_test.txt'
    input_str = _read_input(file_name)
    parsed_data = day_14.process_input(input_str)
    result = day_14.part1(parsed_data)

    expected = 24

    assert result == expected


def test_part2():
    file_name = f'{data_dir}day_{day:02}_test.txt'
    input_str = _read_input(file_name)
    parsed_data = day_14.process_input(input_str)
    result = day_14.part2(parsed_data)

    expected = 93

    assert result == expected
