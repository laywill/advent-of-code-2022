import os
import sys
from pathlib import Path

current_path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, current_path + '/../../')

# from src.common import aoc_file_io
from src.day_03 import day_03  # noqa: E402

# Day of the AOC
day = 3
data_dir = f"data/{day:02}/"
file_name_test = f'{data_dir}day_{day:02}_test.txt'

# Define the example data file and the puzzle input data
INPUT_PATH_TEST = Path.cwd() / f"./{file_name_test}"

if not INPUT_PATH_TEST.is_file():
    raise FileNotFoundError(f"Error: File does not exist: {INPUT_PATH_TEST}")


def test_process_input():
    # Read the files and convert to lists of ints
    with INPUT_PATH_TEST.open(mode="r", encoding="utf-8") as in_file_test:
        report_test = in_file_test.read().strip()

    result = day_03.process_input(report_test)
    # spell-checker:disable
    expected = [
        "vJrwpWtwJgWrhcsFMMfFFhFp",
        "jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL",
        "PmmdzqPrVvPwwTWBwg",
        "wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn",
        "ttgJtRGJQctTZtZT",
        "CrZsJsPPZsGzwwsLwLmpwMDw",
    ]
    # spell-checker:enable

    assert result == expected


def test_part1():
    # Sum of priorities of item common in two pouches
    with INPUT_PATH_TEST.open(mode="r", encoding="utf-8") as in_file_test:
        report_test = in_file_test.read().strip()

    result = day_03.part1(day_03.process_input(report_test))
    expected = 157

    assert result == expected


def test_part2():
    # Sum of priority of item common in set of 3 bags
    with INPUT_PATH_TEST.open(mode="r", encoding="utf-8") as in_file_test:
        report_test = in_file_test.read().strip()

    result = day_03.part2(day_03.process_input(report_test))
    expected = 70

    assert result == expected
