import os
import sys
from pathlib import Path

current_path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, current_path + '/../../')

# from src.common import aoc_file_io
from src.day_11 import day_11  # noqa: E402

# Day of the AOC
day = 11
data_dir = f"data/{day:02}/"

file_name = f'{data_dir}day_{day:02}_test.txt'

# Define the example data file and the puzzle input data
INPUT_DATA_PATH = Path.cwd() / f"./{file_name}"

if not INPUT_DATA_PATH.is_file():
    raise FileNotFoundError(f"Error: File does not exist: {INPUT_DATA_PATH}")

with INPUT_DATA_PATH.open(mode="r", encoding="utf-8") as in_file_data:
    input_str = in_file_data.read()

parsed_data = day_11.process_input(input_str)


def test_process_input():
    result = day_11.process_input(input_str)
    expected = [
        [
            0,
            [
                '  Starting items: 79, 98',
                '  Operation: new = old * 19',
                '  Test: divisible by 23',
                '    If true: throw to monkey 2',
                '    If false: throw to monkey 3',
            ],
        ],
        [
            1,
            [
                '  Starting items: 54, 65, 75, 74',
                '  Operation: new = old + 6',
                '  Test: divisible by 19',
                '    If true: throw to monkey 2',
                '    If false: throw to monkey 0',
            ],
        ],
        [
            2,
            [
                '  Starting items: 79, 60, 97',
                '  Operation: new = old * old',
                '  Test: divisible by 13',
                '    If true: throw to monkey 1',
                '    If false: throw to monkey 3',
            ],
        ],
        [
            3,
            [
                '  Starting items: 74',
                '  Operation: new = old + 3',
                '  Test: divisible by 17',
                '    If true: throw to monkey 0',
                '    If false: throw to monkey 1',
            ],
        ],
    ]

    assert result == expected


def _test_MonkeyBusiness_part1_init():
    mb = day_11.MonkeyBusiness(parsed_data, True)

    assert mb.enable_div_by_3

    assert mb.num_monkeys == 4

    for i in range(mb.num_monkeys):
        assert type(mb.monkeys[i]) is day_11.Monkey


def _test_MonkeyBusiness_part2_init():
    mb = day_11.MonkeyBusiness(parsed_data, False)

    assert not mb.enable_div_by_3

    assert mb.num_monkeys == 4

    for i in range(mb.num_monkeys):
        assert type(mb.monkeys[i]) is day_11.Monkey


def _test_MonkeyBusiness_part1_round_1():
    mb = day_11.MonkeyBusiness(parsed_data, True)
    mb.play_rounds(1)
    assert mb.monkeys[0].items == [20, 23, 27, 26]
    assert mb.monkeys[1].items == [2080, 25, 167, 207, 401, 1046]
    assert mb.monkeys[2].items == []
    assert mb.monkeys[3].items == []


def _test_MonkeyBusiness_part1_round_2():
    mb = day_11.MonkeyBusiness(parsed_data, True)
    mb.play_rounds(2)
    assert mb.monkeys[0].items == [695, 10, 71, 135, 350]
    assert mb.monkeys[1].items == [43, 49, 58, 55, 362]
    assert mb.monkeys[2].items == []
    assert mb.monkeys[3].items == []


def _test_MonkeyBusiness_part1_round_3():
    mb = day_11.MonkeyBusiness(parsed_data, True)
    mb.play_rounds(3)
    assert mb.monkeys[0].items == [16, 18, 21, 20, 122]
    assert mb.monkeys[1].items == [1468, 22, 150, 286, 739]
    assert mb.monkeys[2].items == []
    assert mb.monkeys[3].items == []


def _test_MonkeyBusiness_part1_round_4():
    mb = day_11.MonkeyBusiness(parsed_data, True)
    mb.play_rounds(4)
    assert mb.monkeys[0].items == [491, 9, 52, 97, 248, 34]
    assert mb.monkeys[1].items == [39, 45, 43, 258]
    assert mb.monkeys[2].items == []
    assert mb.monkeys[3].items == []


def _test_MonkeyBusiness_part1_round_5():
    mb = day_11.MonkeyBusiness(parsed_data, True)
    mb.play_rounds(5)
    assert mb.monkeys[0].items == [15, 17, 16, 88, 1037]
    assert mb.monkeys[1].items == [20, 110, 205, 524, 72]
    assert mb.monkeys[2].items == []
    assert mb.monkeys[3].items == []


def _test_MonkeyBusiness_part1_round_6():
    mb = day_11.MonkeyBusiness(parsed_data, True)
    mb.play_rounds(6)
    assert mb.monkeys[0].items == [8, 70, 176, 26, 34]
    assert mb.monkeys[1].items == [481, 32, 36, 186, 2190]
    assert mb.monkeys[2].items == []
    assert mb.monkeys[3].items == []


def _test_MonkeyBusiness_part1_round_7():
    mb = day_11.MonkeyBusiness(parsed_data, True)
    mb.play_rounds(7)
    assert mb.monkeys[0].items == [162, 12, 14, 64, 732, 17]
    assert mb.monkeys[1].items == [148, 372, 55, 72]
    assert mb.monkeys[2].items == []
    assert mb.monkeys[3].items == []


def _test_MonkeyBusiness_part1_round_8():
    mb = day_11.MonkeyBusiness(parsed_data, True)
    mb.play_rounds(8)
    assert mb.monkeys[0].items == [51, 126, 20, 26, 136]
    assert mb.monkeys[1].items == [343, 26, 30, 1546, 36]
    assert mb.monkeys[2].items == []
    assert mb.monkeys[3].items == []


def _test_MonkeyBusiness_part1_round_9():
    mb = day_11.MonkeyBusiness(parsed_data, True)
    mb.play_rounds(9)
    assert mb.monkeys[0].items == [116, 10, 12, 517, 14]
    assert mb.monkeys[1].items == [108, 267, 43, 55, 288]
    assert mb.monkeys[2].items == []
    assert mb.monkeys[3].items == []


def _test_MonkeyBusiness_part1_round_10():
    mb = day_11.MonkeyBusiness(parsed_data, True)
    mb.play_rounds(10)
    assert mb.monkeys[0].items == [91, 16, 20, 98]
    assert mb.monkeys[1].items == [481, 245, 22, 26, 1092, 30]
    assert mb.monkeys[2].items == []
    assert mb.monkeys[3].items == []


def _test_MonkeyBusiness_part1_round_15():
    mb = day_11.MonkeyBusiness(parsed_data, True)
    mb.play_rounds(15)
    assert mb.monkeys[0].items == [83, 44, 8, 184, 9, 20, 26, 102]
    assert mb.monkeys[1].items == [110, 36]
    assert mb.monkeys[2].items == []
    assert mb.monkeys[3].items == []


def _test_MonkeyBusiness_part1_round_20():
    mb = day_11.MonkeyBusiness(parsed_data, True)
    mb.play_rounds(20)
    assert mb.monkeys[0].items == [10, 12, 14, 26, 34]
    assert mb.monkeys[1].items == [245, 93, 53, 199, 115]
    assert mb.monkeys[2].items == []
    assert mb.monkeys[3].items == []

    counts = mb.get_monkey_inspection_counts()
    assert counts[0] == 101
    assert counts[1] == 95
    assert counts[2] == 7
    assert counts[3] == 105

    assert counts[0] * counts[3] == 10605


def _test_MonkeyBusiness_part2_round_1(game):
    game.play_rounds(1)
    assert game.num_rounds_played == 1
    counts = game.get_monkey_inspection_counts()
    assert counts[0] == 2
    assert counts[1] == 4
    assert counts[2] == 3
    assert counts[3] == 6


def _test_MonkeyBusiness_part2_round_20(game):
    game.play_rounds(19)
    assert game.num_rounds_played == 20
    counts = game.get_monkey_inspection_counts()
    assert counts[0] == 99
    assert counts[1] == 97
    assert counts[2] == 8
    assert counts[3] == 103


def _test_MonkeyBusiness_part2_round_1000(game):
    game.play_rounds(980)
    assert game.num_rounds_played == 1000
    counts = game.get_monkey_inspection_counts()
    assert counts[0] == 5204
    assert counts[1] == 4792
    assert counts[2] == 199
    assert counts[3] == 5192


def _test_MonkeyBusiness_part2_round_2000(game):
    game.play_rounds(1000)
    assert game.num_rounds_played == 2000
    counts = game.get_monkey_inspection_counts()
    assert counts[0] == 10419
    assert counts[1] == 9577
    assert counts[2] == 392
    assert counts[3] == 10391


def _test_MonkeyBusiness_part2_round_3000(game):
    game.play_rounds(1000)
    assert game.num_rounds_played == 3000
    counts = game.get_monkey_inspection_counts()
    assert counts[0] == 15638
    assert counts[1] == 14358
    assert counts[2] == 587
    assert counts[3] == 15593


def _test_MonkeyBusiness_part2_round_4000(game):
    game.play_rounds(1000)
    assert game.num_rounds_played == 4000
    counts = game.get_monkey_inspection_counts()
    assert counts[0] == 20858
    assert counts[1] == 19138
    assert counts[2] == 780
    assert counts[3] == 20797


def _test_MonkeyBusiness_part2_round_5000(game):
    game.play_rounds(1000)
    assert game.num_rounds_played == 5000
    counts = game.get_monkey_inspection_counts()
    assert counts[0] == 26075
    assert counts[1] == 23921
    assert counts[2] == 974
    assert counts[3] == 26000


def _test_MonkeyBusiness_part2_round_6000(game):
    game.play_rounds(1000)
    assert game.num_rounds_played == 6000
    counts = game.get_monkey_inspection_counts()
    assert counts[0] == 31294
    assert counts[1] == 28702
    assert counts[2] == 1165
    assert counts[3] == 31204


def _test_MonkeyBusiness_part2_round_7000(game):
    game.play_rounds(1000)
    assert game.num_rounds_played == 7000
    counts = game.get_monkey_inspection_counts()
    assert counts[0] == 36508
    assert counts[1] == 33488
    assert counts[2] == 1360
    assert counts[3] == 36400


def _test_MonkeyBusiness_part2_round_8000(game):
    game.play_rounds(1000)
    assert game.num_rounds_played == 8000
    counts = game.get_monkey_inspection_counts()
    assert counts[0] == 41728
    assert counts[1] == 38268
    assert counts[2] == 1553
    assert counts[3] == 41606


def _test_MonkeyBusiness_part2_round_9000(game):
    game.play_rounds(1000)
    assert game.num_rounds_played == 9000
    counts = game.get_monkey_inspection_counts()
    assert counts[0] == 46945
    assert counts[1] == 43051
    assert counts[2] == 1746
    assert counts[3] == 46807


def _test_MonkeyBusiness_part2_round_10000(game):
    game.play_rounds(1000)
    assert game.num_rounds_played == 10000
    counts = game.get_monkey_inspection_counts()
    assert counts[0] == 52166
    assert counts[1] == 47830
    assert counts[2] == 1938
    assert counts[3] == 52013

    assert counts[0] * counts[3] == 2713310158


def test_MonkeyBusiness():
    _test_MonkeyBusiness_part1_init()
    _test_MonkeyBusiness_part2_init()
    _test_MonkeyBusiness_part1_round_1()
    _test_MonkeyBusiness_part1_round_2()
    _test_MonkeyBusiness_part1_round_3()
    _test_MonkeyBusiness_part1_round_4()
    _test_MonkeyBusiness_part1_round_5()
    _test_MonkeyBusiness_part1_round_6()
    _test_MonkeyBusiness_part1_round_7()
    _test_MonkeyBusiness_part1_round_8()
    _test_MonkeyBusiness_part1_round_9()
    _test_MonkeyBusiness_part1_round_10()
    _test_MonkeyBusiness_part1_round_15()
    _test_MonkeyBusiness_part1_round_20()

    p2_mb = day_11.MonkeyBusiness(parsed_data, False)
    # p2_mb.enable_mod_lcm()
    _test_MonkeyBusiness_part2_round_1(p2_mb)
    _test_MonkeyBusiness_part2_round_20(p2_mb)
    _test_MonkeyBusiness_part2_round_1000(p2_mb)
    _test_MonkeyBusiness_part2_round_2000(p2_mb)
    _test_MonkeyBusiness_part2_round_3000(p2_mb)
    _test_MonkeyBusiness_part2_round_4000(p2_mb)
    _test_MonkeyBusiness_part2_round_5000(p2_mb)
    _test_MonkeyBusiness_part2_round_6000(p2_mb)
    _test_MonkeyBusiness_part2_round_7000(p2_mb)
    _test_MonkeyBusiness_part2_round_8000(p2_mb)
    _test_MonkeyBusiness_part2_round_9000(p2_mb)
    _test_MonkeyBusiness_part2_round_10000(p2_mb)


def test_part1():
    result = day_11.part1(parsed_data)

    expected = 10605

    assert result == expected


def test_part2():
    result = day_11.part2(parsed_data)

    expected = 2713310158

    assert result == expected
