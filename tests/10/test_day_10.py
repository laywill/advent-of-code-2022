import os
import sys
from pathlib import Path

current_path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, current_path + '/../../')

# from src.common import aoc_file_io
from src.day_10 import day_10  # noqa: E402

# Day of the AOC
day = 10
data_dir = f"data/{day:02}/"


def _read_input(data_filepath):
    # Define the example data file and the puzzle input data
    INPUT_PATH_TEST = Path.cwd() / f"./{data_filepath}"

    if not INPUT_PATH_TEST.is_file():
        raise FileNotFoundError(
            f"Error: File does not exist: {INPUT_PATH_TEST}"
        )

    with INPUT_PATH_TEST.open(mode="r", encoding="utf-8") as in_file_test:
        input_test_str = in_file_test.read()

    return input_test_str


def test_process_input():
    file_name = f'{data_dir}day_{day:02}_test_01.txt'
    input_str = _read_input(file_name)
    result = day_10.process_input(input_str)
    expected = [["noop"], ["addx", 3], ["addx", -5]]

    assert result == expected


def test_part1():
    """Can the algorithm successfully get the same answer as AOC example."""
    file_name = f'{data_dir}day_{day:02}_test_02.txt'
    input_str = _read_input(file_name)
    parsed_data = day_10.process_input(input_str)
    result = day_10.part1(parsed_data)

    expected = 13140

    assert result == expected


def test_part2():
    """Can the algorithm find the shortest number of steps for any valid start."""
    file_name = f'{data_dir}day_{day:02}_test_02.txt'
    input_str = _read_input(file_name)
    parsed_data = day_10.process_input(input_str)
    result = day_10.part2(parsed_data)

    expected = [
        "##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ",
        "###   ###   ###   ###   ###   ###   ### ",
        "####    ####    ####    ####    ####    ",
        "#####     #####     #####     #####     ",
        "######      ######      ######      ####",
        "#######       #######       #######     ",
    ]

    assert result == expected
