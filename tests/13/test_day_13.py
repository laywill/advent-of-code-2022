import os
import sys
from pathlib import Path

import numpy as np

current_path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, current_path + '/../../')

# from src.common import aoc_file_io
from src.day_13 import day_13  # noqa: E402

# Day of the AOC
day = 13
data_dir = f"data/{day:02}/"


def _read_input(data_filepath):
    # Define the example data file and the puzzle input data
    INPUT_PATH_TEST = Path.cwd() / f"./{data_filepath}"

    if not INPUT_PATH_TEST.is_file():
        raise FileNotFoundError(
            f"Error: File does not exist: {INPUT_PATH_TEST}"
        )

    with INPUT_PATH_TEST.open(mode="r", encoding="utf-8") as in_file_test:
        input_test_str = in_file_test.read()

    return input_test_str


def test_process_input_part1():
    file_name = f'{data_dir}day_{day:02}_test.txt'
    input_str = _read_input(file_name)
    result = day_13.process_input_part1(input_str)
    expected = [
        {"left": [1, 1, 3, 1, 1], "right": [1, 1, 5, 1, 1]},
        {"left": [[1], [2, 3, 4]], "right": [[1], 4]},
        {"left": [9], "right": [[8, 7, 6]]},
        {"left": [[4, 4], 4, 4], "right": [[4, 4], 4, 4, 4]},
        {"left": [7, 7, 7, 7], "right": [7, 7, 7]},
        {"left": [], "right": [3]},
        {"left": [[[]]], "right": [[]]},
        {
            "left": [1, [2, [3, [4, [5, 6, 7]]]], 8, 9],
            "right": [1, [2, [3, [4, [5, 6, 0]]]], 8, 9],
        },
    ]

    # assert all(map(lambda x, y: x == y, result, expected))
    assert result == expected


def test_process_input_part2():
    file_name = f'{data_dir}day_{day:02}_test.txt'
    input_str = _read_input(file_name)
    result = day_13.process_input_part2(input_str)
    expected = [
        [1, 1, 3, 1, 1],
        [1, 1, 5, 1, 1],
        [[1], [2, 3, 4]],
        [[1], 4],
        [9],
        [[8, 7, 6]],
        [[4, 4], 4, 4],
        [[4, 4], 4, 4, 4],
        [7, 7, 7, 7],
        [7, 7, 7],
        [],
        [3],
        [[[]]],
        [[]],
        [1, [2, [3, [4, [5, 6, 7]]]], 8, 9],
        [1, [2, [3, [4, [5, 6, 0]]]], 8, 9],
    ]

    # assert all(map(lambda x, y: x == y, result, expected))
    assert result == expected


def _test_is_correct_order_list_int_1():
    test_packet_l = [[0, 0, 0]]
    test_packet_r = [2]

    ls = day_13.List_Solver([{"left": test_packet_l, "right": test_packet_r}])
    ls.run_solver()
    result = ls.correct_order

    expected = True

    assert result == expected


def _test_is_correct_order_list_int_2():
    test_packet_l = [[2, 2, 2]]
    test_packet_r = [0]

    ls = day_13.List_Solver([{"left": test_packet_l, "right": test_packet_r}])
    ls.run_solver()
    result = ls.correct_order

    expected = False

    assert result == expected


def _test_is_correct_order_int_list_1():
    test_packet_l = [0]
    test_packet_r = [[9, 9, 9]]

    ls = day_13.List_Solver([{"left": test_packet_l, "right": test_packet_r}])
    ls.run_solver()
    result = ls.correct_order

    expected = True

    assert result == expected


def _test_is_correct_order_int_list_2():
    test_packet_l = [9]
    test_packet_r = [[0, 1, 2]]

    ls = day_13.List_Solver([{"left": test_packet_l, "right": test_packet_r}])
    ls.run_solver()
    result = ls.correct_order

    expected = False

    assert result == expected


def _test_is_correct_order_pair_1():
    test_packet_l = [1, 1, 3, 1, 1]
    test_packet_r = [1, 1, 5, 1, 1]

    ls = day_13.List_Solver([{"left": test_packet_l, "right": test_packet_r}])
    ls.run_solver()
    result = ls.correct_order

    expected = True

    assert result == expected


def _test_is_correct_order_pair_2():
    test_packet_l = [[1], [2, 3, 4]]
    test_packet_r = [[1], 4]

    ls = day_13.List_Solver([{"left": test_packet_l, "right": test_packet_r}])
    ls.run_solver()
    result = ls.correct_order

    expected = True

    assert result == expected


def _test_is_correct_order_pair_3():
    test_packet_l = [9]
    test_packet_r = [[8, 7, 6]]

    ls = day_13.List_Solver([{"left": test_packet_l, "right": test_packet_r}])
    ls.run_solver()
    result = ls.correct_order

    expected = False

    assert result == expected


def _test_is_correct_order_pair_4():
    test_packet_l = [[4, 4], 4, 4]
    test_packet_r = [[4, 4], 4, 4, 4]

    ls = day_13.List_Solver([{"left": test_packet_l, "right": test_packet_r}])
    ls.run_solver()
    result = ls.correct_order

    expected = True

    assert result == expected


def _test_is_correct_order_pair_5():
    test_packet_l = [7, 7, 7, 7]
    test_packet_r = [7, 7, 7]

    ls = day_13.List_Solver([{"left": test_packet_l, "right": test_packet_r}])
    ls.run_solver()
    result = ls.correct_order

    expected = False

    assert result == expected


def _test_is_correct_order_pair_6():
    test_packet_l = []
    test_packet_r = [3]

    ls = day_13.List_Solver([{"left": test_packet_l, "right": test_packet_r}])
    ls.run_solver()
    result = ls.correct_order

    expected = True

    assert result == expected


def _test_is_correct_order_pair_7():
    test_packet_l = [[[]]]
    test_packet_r = [[]]

    ls = day_13.List_Solver([{"left": test_packet_l, "right": test_packet_r}])
    ls.run_solver()
    result = ls.correct_order

    expected = False

    assert result == expected


def _test_is_correct_order_pair_8():
    test_packet_l = [1, [2, [3, [4, [5, 6, 7]]]], 8, 9]
    test_packet_r = [1, [2, [3, [4, [5, 6, 0]]]], 8, 9]

    ls = day_13.List_Solver([{"left": test_packet_l, "right": test_packet_r}])
    ls.run_solver()
    result = ls.correct_order

    expected = False

    assert result == expected


def _test_is_correct_order_fail_a_deep_nest():
    test_packet_l = [[[[[[[[[1]]]]]]]]]
    test_packet_r = [[[[[[[[[0]]]]]]]]]

    ls = day_13.List_Solver([{"left": test_packet_l, "right": test_packet_r}])
    ls.run_solver()
    result = ls.correct_order

    expected = False

    assert result == expected


def _test_is_correct_order_success_a_deep_nest():
    test_packet_l = [[[[[[[[[0]]]]]]]]]
    test_packet_r = [[[[[[[[[1]]]]]]]]]

    ls = day_13.List_Solver([{"left": test_packet_l, "right": test_packet_r}])
    ls.run_solver()
    result = ls.correct_order

    expected = True

    assert result == expected


def _test_is_correct_order_line_433():
    """Returns False.

    Initially compares [4,4,4] to [10,4,7 ...].
    List to int, so cast 10 to [10]
    Comparing [4,4,4] to [10].
    Left list runs out of items first, so in right order.
    """
    test_packet_l = [
        [4, 4, 4],
        [1, [10, [], 10, 6, [8, 8, 3, 7, 7]], 6],
        [[0, 8, [9], 4, [3, 6]], [[4, 5, 3, 6, 8], 4]],
        [1, [[3], []]],
    ]
    test_packet_r = [
        [10, 4, 7, [[8, 9], 7, 7, [10, 1, 2], 10], [[9, 8], 1]],
        [],
        [[4, 10, []]],
    ]

    ls = day_13.List_Solver([{"left": test_packet_l, "right": test_packet_r}])
    ls.run_solver()
    result = ls.correct_order

    expected = True

    assert result == expected


def _test_is_correct_order_line_97():
    """Return False

    Initially compare [] to []
    Inconclusive.

    Compare [[[3], 10, 3, 7]] to [[], 4, [2, [0], 0, []], 4]
    Move to sublists:
    Compare [[3], 10, 3, 7] to []
    Right runs out of elements first, so in wrong order.
    """
    test_packet_l = [[], [[[3], 10, 3, 7]], []]
    test_packet_r = [
        [],
        [[], 4, [2, [0], 0, []], 4],
        [],
        [[5, 0, 10, 6, 9], 2],
    ]

    ls = day_13.List_Solver([{"left": test_packet_l, "right": test_packet_r}])
    ls.run_solver()
    result = ls.correct_order

    expected = False

    assert result == expected


def test_is_correct_order():
    _test_is_correct_order_list_int_1()
    _test_is_correct_order_list_int_2()
    _test_is_correct_order_int_list_1()
    _test_is_correct_order_int_list_2()
    _test_is_correct_order_pair_1()
    _test_is_correct_order_pair_2()
    _test_is_correct_order_pair_3()
    _test_is_correct_order_pair_4()
    _test_is_correct_order_pair_5()
    _test_is_correct_order_pair_6()
    _test_is_correct_order_pair_7()
    _test_is_correct_order_pair_8()
    _test_is_correct_order_fail_a_deep_nest()
    _test_is_correct_order_success_a_deep_nest()
    _test_is_correct_order_line_433()
    _test_is_correct_order_line_97()


def _test_part1_test():
    file_name = f'{data_dir}day_{day:02}_test.txt'
    input_str = _read_input(file_name)
    result = day_13.part1(input_str)

    expected = 13

    assert result == expected


def _test_part1_real():
    file_name = f'{data_dir}day_{day:02}_real.txt'
    input_str = _read_input(file_name)
    result = day_13.part1(input_str)

    expected = 5843

    assert result == expected


def test_part1():
    _test_part1_test()
    _test_part1_real()


def _test_part2_test():
    """Can the algorithm find the shortest number of steps for any valid start."""
    file_name = f'{data_dir}day_{day:02}_test.txt'
    input_str = _read_input(file_name)
    result = day_13.part2(input_str)

    expected = 140

    assert result == expected


def _test_part2_real():
    """Can the algorithm find the shortest number of steps for any valid start."""
    file_name = f'{data_dir}day_{day:02}_real.txt'
    input_str = _read_input(file_name)
    result = day_13.part2(input_str)

    expected = 26289

    assert result == expected


def test_part2():
    _test_part2_test()
    _test_part2_real()
