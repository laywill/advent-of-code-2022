# Advent Of Code 2021

Advent of Code is an annual Advent calendar of small programming puzzles for a variety of skill sets and skill levels that can be solved in any programming language you like.

[Advent of Code 2022 Website](https://adventofcode.com/2022/)

## Project status

[![pipeline status](https://gitlab.com/laywill/advent-of-code-2022/badges/main/pipeline.svg)](https://gitlab.com/laywill/advent-of-code-2022/-/commits/main)
[![coverage report](https://gitlab.com/laywill/advent-of-code-2022/badges/main/coverage.svg)](https://gitlab.com/laywill/advent-of-code-2022/-/commits/main)

Active

<!-- If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers. -->

### Progress

| Day | Part 1 | Part 2 |
|----:|:------:|:------:|
|   1 | :star: | :star: |
|   2 | :star: | :star: |
|   3 | :star: | :star: |
|   4 | :star: | :star: |
|   5 | :star: | :star: |
|   6 | :star: | :star: |
|   7 | :star: | :star: |
|   8 | :star: | :star: |
|   9 | :star: | :star: |
|  10 | :star: | :star: |
|  11 | :star: | :star: |
|  12 | :star: | :star: |
|  13 | :star: | :star: |
|  14 | :star: | :star: |
|  15 | :star: | :star: |
|  16 |        |        |
|  17 |        |        |
|  18 |        |        |
|  19 |        |        |
|  20 |        |        |
|  21 |        |        |
|  22 |        |        |
|  23 |        |        |
|  24 |        |        |
|  25 |        |        |

## Folder Structure

Making use of out-of-tree execution helps to preserve a clean source tree. This isn't essential when using languages like Python and Bash, but becomes helpful when using compiled languages like C/C++.

Rather than a folder per day, instead we separate by purpose, then by day.

```plaintext
/data         - All supplied data for AoC puzzles
/data/day_XX  - Data for a specific day
/src          - All source code
/src/day_XX   - Source code for a specific day
/src/common   - Shared libraries and components
/scripts      - Supporting scripts, e.g. for setup, CI etc.
/tests        - All test files
/tests/day_XX - Tests for a specific day
/output       - Out-of-tree build area, and output generation
```

## Setup and Use

### Python Version

This has been developed using Python 3.11

### Create a virtual environment

On MacOS and Linux:

```bash
python3 -m venv venv
```

On Windows:

```powershell
python -m venv venv
```

### Activate virtual environment

On MacOS and Linux:

```bash
source venv/bin/activate
```

On Windows:

```powershell
.\venv\Scripts\activate
```

### Install packages from requirements

Tell pip to install all of the packages in this file using the -r flag:

```bash
pip install -r requirements.txt
```

### Install and configure git pre-commit

To install git hooks in your .git/ directory, execute:

```bash
pre-commit install
```

### Running checks on repository

If you want to run the checks on-demand (outside of git hooks), run:

```bash
pre-commit run --all-files --verbose
```

### Running Scripts

All scripts assume you are running them from the root of the repository.
Scripts do not take any parameters, and are invoked directly e.g.:

```bash
python ./src/day1.py
```

### Leaving the virtual environment

Simply type:

```bash
deactivate
```

## Development

### Updating requirements

To update `requirements.txt` use the following command:

```bash
pip freeze > requirements.txt
```

### Upgrading requirements.txt

To automagically upgrade the packages in requirements.txt run

```bash
pip freeze | %{$_.split('==')[0]} | %{pip install --upgrade $_}
```

<!-- ## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://gitlab.com/-/experiment/new_project_readme_content:dff124e66fc67439ec3acf77213a3309?https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://gitlab.com/-/experiment/new_project_readme_content:dff124e66fc67439ec3acf77213a3309?https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://gitlab.com/-/experiment/new_project_readme_content:dff124e66fc67439ec3acf77213a3309?https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://gitlab.com/-/experiment/new_project_readme_content:dff124e66fc67439ec3acf77213a3309?https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://gitlab.com/-/experiment/new_project_readme_content:dff124e66fc67439ec3acf77213a3309?https://docs.gitlab.com/ee/ci/environments/protected_environments.html) -->

## Authors and acknowledgement

William Lay
