#!/usr/bin/env python3.11
"""Solution to Day 13 of AoC 2022."""

import ast
from pathlib import Path


def process_input_part1(input_string: str):
    """Return list of dicts of lists."""
    input_as_list_of_str = input_string.strip().split("\n\n")
    data_pairs = []
    for idx, val in enumerate(input_as_list_of_str):
        lines = val.strip().split("\n")

        # Why use ast.literal_eval() instead of eval()...?
        # What it does:
        #   - Evaluate an expression node or a string containing only a Python literal or container display.
        # Why use it:
        # 1) This can be used for evaluating strings containing Python values without the need to parse the values oneself.
        # 2) This is specifically designed not to execute Python code, unlike the more general eval().
        line1 = ast.literal_eval(lines[0])
        line2 = ast.literal_eval(lines[1])

        data_pairs.append({"left": line1, "right": line2})
    return data_pairs


def process_input_part2(input_string: str):
    """Return a list of strings"""
    input_lines = input_string.replace("\n\n", "\n").strip().split("\n")
    data_packets = []

    for _, val in enumerate(input_lines):
        # Why use ast.literal_eval() instead of eval()...?
        # What it does:
        #   - Evaluate an expression node or a string containing only a Python literal or container display.
        # Why use it:
        # 1) This can be used for evaluating strings containing Python values without the need to parse the values oneself.
        # 2) This is specifically designed not to execute Python code, unlike the more general eval().
        data_packets.append(ast.literal_eval(val))
    return data_packets


class List_Solver:
    def __init__(self, list_of_packets: list):
        """Initialise control logic."""

        # Packets in this list
        self.packets = list_of_packets

        # Index of current packet being investigated
        self.packet_idx = 0

        # For this list, is it in the correct order
        self.correct_order = False

        # Can we trust the correct order marker?
        self.correct_order_valid = False

        # Store the indexes of the correctly ordered packets here
        self.correct_packets = []

    def get_types(self, ip_left, ip_right) -> dict:
        """Returns a dict with the data type of the two inputs."""
        type_left = type(ip_left)
        type_right = type(ip_right)
        return {"left": type_left, "right": type_right}

    def compare_two_ints(self, left_int: int, right_int: int) -> str:
        """Return if ints are in correct order and we should keep checking this packet.

        If both values are integers, the lower integer should come first.
        If the left integer is lower than the right integer, the inputs are in the right order.
        If the left integer is higher than the right integer, the inputs are not in the right order.
        Otherwise, the inputs are the same integer; continue checking the next part of the input.
        """
        if left_int < right_int:
            return "L<R"
        elif left_int == right_int:
            return "L==R"
        else:
            return "L>R"

    def run_sub_solver(self, list_for_solver) -> bool:
        sub_solver = List_Solver(list_for_solver)
        sub_solver.run_solver()

        if sub_solver.correct_order_valid:
            self.correct_order = sub_solver.correct_order
            self.correct_order_valid = True
            return self.correct_order

    def is_correct_order(self, left_packet, right_packet) -> bool:
        """Return if lists are in correct order, and we should keep checking this packet.

        If both values in the list are ints, compare the integers.
        If both values are lists, compare the first value of each list, then the second value, and so on.
        If the left list runs out of items first, the inputs are in the right order.
        If the right list runs out of items first, the inputs are not in the right order.
        If the lists are the same length and no comparison makes a decision about the order, continue checking the next part of the input.
        """
        # If the left list runs out of items first, the inputs are in the right order.
        # Anything that makes it here hasn't failed elsewhere and completed enumerating the left list.
        # Therefore must be valid.
        if (len(left_packet) == 0) and (len(left_packet) < len(right_packet)):
            self.correct_order = True
            self.correct_order_valid = True
            return self.correct_order

        for i, _ in enumerate(left_packet):
            try:
                types = self.get_types(left_packet[i], right_packet[i])
            except IndexError:
                # If this is thrown, we know: len(right_packet) < len(left_packet):
                self.correct_order = False
                self.correct_order_valid = True
                return self.correct_order

            if (types["left"] == int) and (types["right"] == int):
                comparison_result = self.compare_two_ints(
                    left_packet[i], right_packet[i]
                )
                if comparison_result == "L<R":
                    self.correct_order = True
                    self.correct_order_valid = True
                    return self.correct_order
                elif comparison_result == "L==R":
                    # Don't return True here!
                    # If two ints are equal, then we can't conclude if a packet is successful
                    # We have to keep looking
                    pass
                elif comparison_result == "L>R":
                    self.correct_order = False
                    self.correct_order_valid = True
                    return self.correct_order
                else:
                    raise ValueError(
                        "Not sure how to process comparison_result",
                        comparison_result,
                    )

            # For List comparisons, recurse and call another solver to compare those sub-lists.
            # If we get a definitive answer from a sub-list, then break and return that answer here.
            elif (types["left"] == list) and (types["right"] == list):
                list_to_solve = [
                    {"left": left_packet[i], "right": right_packet[i]}
                ]

                # Genuinely not a clue why this doesn't work when replaced with:
                # return self.run_sub_solver(list_to_solve)
                # All the other list comparisons work... god only knows
                sub_solver = List_Solver(list_to_solve)
                sub_solver.run_solver()
                if sub_solver.correct_order_valid:
                    self.correct_order = sub_solver.correct_order
                    self.correct_order_valid = True
                    return self.correct_order

            elif (types["left"] == int) and (types["right"] == list):
                list_to_solve = [
                    {"left": [left_packet[i]], "right": right_packet[i]}
                ]
                return self.run_sub_solver(list_to_solve)
            elif (types["left"] == list) and (types["right"] == int):
                list_to_solve = [
                    {"left": left_packet[i], "right": [right_packet[i]]}
                ]
                return self.run_sub_solver(list_to_solve)

            # Can only process ints and lists
            else:
                raise TypeError("Not sure how to process these types:", types)

            # If the left list runs out of items first, the inputs are in the right order.
            # Anything that makes it here hasn't failed elsewhere and completed enumerating the left list.
            # Therefore must be valid.
            if i == len(left_packet) - 1:
                self.correct_order = True
                self.correct_order_valid = True
                return self.correct_order

        self.correct_order_valid = False
        return self.correct_order

    def run_solver(self):
        for self.packet_idx, val in enumerate(self.packets):
            if self.is_correct_order(val["left"], val["right"]):
                self.correct_packets.append(self.packet_idx)


def part1(input_data) -> int:
    """Return the sum of the indices of pairs of packets already in the right order."""
    parsed_data = process_input_part1(input_data)
    solver = List_Solver(parsed_data)

    solver.run_solver()

    # The list of correct packets is 1-indexed, but our list is 0-indexed, so add offset
    correct_indices = [x + 1 for x in solver.correct_packets]
    print("\nCorrect_packets", correct_indices)

    return sum(correct_indices)


def sort_fn(value, depth=1):
    """Function used to sort the lists by first element."""
    if type(value) is int:
        return value
    elif type(value) is list:
        # For empty lists, sort multiple levels as less important than few levels
        if len(value):
            return sort_fn(value[0], 0.5 * depth)
        else:
            return -1 * depth


def get_first_elem(value):
    """Return the first integer from an arbitrarily deep nested list."""
    if type(value) is int:
        return value
    elif type(value) is list:
        # For empty lists, sort multiple levels as less important than few levels
        if len(value):
            return get_first_elem(value[0])
        else:
            return None


def gen_data_pairs(list_of_packets):
    """Take a list of packets, and pair them up in the format the solver expects."""
    op_data_pairs = []
    for idx in range(0, len(list_of_packets), 2):
        op_data_pairs.append(
            {"left": list_of_packets[idx], "right": list_of_packets[idx + 1]}
        )
    return op_data_pairs


def swap_positions(list, pos1, pos2):
    """Swap two items in a list."""
    list[pos1], list[pos2] = list[pos2], list[pos1]
    return list


def part2(input_data) -> int:
    """Return the decoder key for the distress signal.

    To find the decoder key for this distress signal, multiply the indices of the two divider packets together.
    """
    parsed_data = process_input_part2(input_data)

    # Using the same rules as before, organize all packets
    # (the ones in your list of received packets as well as the two divider packets)
    # into the correct order.
    # Start by doing a rough sort by initial character
    parsed_data.sort(key=sort_fn)

    # The distress signal protocol also requires that you include two additional divider packets:
    # [[2]] and [[6]]
    # Since we've just sorted the list nicely, we will insert these as the first "2" and "6" items
    # Hopefully this saves on rearranging.
    inserted_2 = False
    inserted_6 = False
    insert_2_at_idx = 0
    insert_6_at_idx = 0
    for i, v in enumerate(parsed_data):
        # Pull the first element from the next item of parsed data
        # If it is None then it is an empty list, move on
        # If it is an int, then look at the value, and store if what we want.
        # We want to get the index to inset the [[2]] and [[6]] as the first with that integer.
        result = get_first_elem(v)
        if type(result) is int:
            if result < 2:
                insert_2_at_idx = i + 1
            elif (result == 2) and not inserted_2:
                insert_2_at_idx = i
                inserted_2 = True
            elif (result >= 3) and not inserted_2:
                insert_2_at_idx = i - 1
                inserted_2 = True

            if result < 6:
                insert_6_at_idx = i + 1
            elif (result == 6) and not inserted_6:
                insert_6_at_idx = i
                inserted_6 = True
            elif (result >= 7) and not inserted_6:
                insert_6_at_idx = i - 1
                inserted_6 = True

    # Insert from the end of the list first, so smaller indicies are still valid
    print("Insert 2 at:", insert_2_at_idx)
    print("Insert 6 at:", insert_6_at_idx)
    parsed_data.insert(insert_6_at_idx, [[6]])
    parsed_data.insert(insert_2_at_idx, [[2]])

    # Convert the sorted list into data pairs that solver can process
    data_pairs = gen_data_pairs(parsed_data)

    # We have no known correct pairs, and a target of having all pairs be valid
    target_matching_pairs = len(data_pairs)
    correct_pairs = 0

    # Keep running the solver, swapping adjacent pairs that are incorrect.
    # Since the list is sorted we shouldn't need to swap much, anyway...
    iteration = 0
    while True:
        iteration += 1
        solver = List_Solver(data_pairs)
        solver.run_solver()

        correct_pairs = len(solver.correct_packets)

        # When the number of pairs that had a valid solution is the same as the max number of pairs, we are solved.
        if correct_pairs == target_matching_pairs:
            break
        else:
            for i in range(len(data_pairs)):
                if i not in solver.correct_packets:
                    swap_positions(parsed_data, (i * 2), ((i * 2) + 1))

            data_pairs = gen_data_pairs(parsed_data)

    print("Number of iterations required:", iteration)

    # The index is 1-indexed, so add an offset.
    # Indexing for part 2 does not account for data pairs.
    loc_2 = parsed_data.index([[2]]) + 1
    loc_6 = parsed_data.index([[6]]) + 1

    print("index of [[2]]:", loc_2)
    print("index of [[6]]:", loc_6)

    return loc_2 * loc_6


if __name__ == "__main__":
    print("NB: Run this program from the root directory of the repository.\n")

    # Day of the AOC
    day = 13
    data_dir = f"data/{day:02}/"
    # file_name = f'{data_dir}day_{day:02}_test.txt'
    file_name = f'{data_dir}day_{day:02}_real.txt'

    # Define the example data file and the puzzle input data
    INPUT_DATA_PATH = Path.cwd() / f"./{file_name}"

    if not INPUT_DATA_PATH.is_file():
        raise FileNotFoundError(
            f"Error: File does not exist: {INPUT_DATA_PATH}"
        )

    with INPUT_DATA_PATH.open(mode="r", encoding="utf-8") as in_file_data:
        input_str = in_file_data.read()

    print(f"Day {day:02} - Part 1:", part1(input_str))
    print(f"Day {day:02} - Part 2:", part2(input_str))
