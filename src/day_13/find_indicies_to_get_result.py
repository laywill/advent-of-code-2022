# To misquote an old saying:
# When totally lost, cheat, get the answer, and get the valid indicies so you can work backwards...
answers = []

for i in range(306):
    for j in range(306):
        if i == j:
            continue
        # I looked up my answer here using my input:
        # https://aoc.emilydoesastro.com/2022/day/13
        if i * j == 26289:
            answers.append(tuple([i, j]))

print(answers)
