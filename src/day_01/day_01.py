#!/usr/bin/env python3.11
"""Solution to Day 1 of AoC 2022."""

from pathlib import Path


def process_input(input_str):
    """From input string, return a list of elves, for each elf, list of snack calorie values."""
    list_of_calories = list(input_str.split("\n\n"))

    list_of_lists_of_calories = []
    for string in list_of_calories:
        list_of_lists_of_calories.append(string.split("\n"))
        list_of_lists_of_calories[-1] = list(
            map(int, list_of_lists_of_calories[-1])
        )

    # Convert to list of strings
    return list_of_lists_of_calories


def total_calories_per_elf(list_of_lists_of_calories):
    """Find the total calories each elf is carrying, from list of snacks per elf."""
    elf_calories = []
    for elf in list_of_lists_of_calories:
        elf_calories.append(sum(elf))
    return elf_calories


def part1(elf_calories):
    """Return the maximum number of calories any one elf has."""
    return max(elf_calories)


def part2(elf_calories):
    """Return the total calories carried by the top three elves."""
    elf_calories.sort()
    return sum(elf_calories[-3:])


if __name__ == "__main__":
    print("NB: Run this program from the root directory of the repository.\n")

    # Day of the AOC
    day = 1
    data_dir = f"data/{day:02}/"
    # file_name = f'{data_dir}day_{day:02}_test.txt'
    file_name = f'{data_dir}day_{day:02}_real.txt'

    # Define the example data file and the puzzle input data
    INPUT_DATA_PATH = Path.cwd() / f"./{file_name}"

    if not INPUT_DATA_PATH.is_file():
        raise FileNotFoundError(
            f"Error: File does not exist: {INPUT_DATA_PATH}"
        )

    with INPUT_DATA_PATH.open(mode="r", encoding="utf-8") as in_file_data:
        input_str = in_file_data.read().strip()
    calories_list_data = process_input(input_str)

    calories_per_elf_data = total_calories_per_elf(calories_list_data)

    print(f"Day {day:02} - Part 1:", part1(calories_per_elf_data))
    print(f"Day {day:02} - Part 2:", part2(calories_per_elf_data))
