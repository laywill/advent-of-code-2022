#!/usr/bin/env python3.11
"""Solution to Day 6 of AoC 2022."""

from pathlib import Path


def slice_is_start_of_packet(slice):
    """Return true if string slice is 4 unique characters."""
    # Start by validating what we are doing
    if type(slice) is not str:
        raise TypeError(
            "slice_is_start_of_packet was passed an object that is not a string:",
            slice,
        )
    if not len(slice) == 4:
        raise ValueError(
            "Length of slice passed to slice_is_start_of_packet is not 4",
            len(slice),
        )

    # If all 4 characters are different, it is a start of packet
    # The set only contains unique values
    # So if the length of the set is the same as the length of the slice, we know its a start
    if len(set(slice)) == 4:
        return True

    return False


def slice_is_start_of_message(slice):
    """Return true if string slice is 14 unique characters."""
    # Start by validating what we are doing
    if type(slice) is not str:
        raise TypeError(
            "slice_is_start_of_packet was passed an object that is not a string:",
            slice,
        )
    if not len(slice) == 14:
        raise ValueError(
            "Length of slice passed to slice_is_start_of_packet is not 14",
            len(slice),
        )

    # If all 4 characters are different, it is a start of packet
    # The set only contains unique values
    # So if the length of the set is the same as the length of the slice, we know its a start
    if len(set(slice)) == 14:
        return True

    return False


def part1(datastream):
    """Return how many characters need to be processed before the first start-of-packet marker is detected.

    A start of a packet is indicated by a sequence of four characters that are all different.
    The return value should be the 1-indexed value of the last character in the start-marker.
    """
    stop_index = len(datastream) - 4
    for index, value in enumerate(datastream):
        # Make sure our slice won't overrun the end of the datastream
        if index > stop_index:
            break
        if slice_is_start_of_packet(datastream[index : index + 4]):
            return index + 4


def part2(datastream):
    """Return how many characters need to be processed before the first start-of-message marker is detected.

    A start-of-message marker is just like a start-of-packet marker, except it consists of 14 distinct characters rather than 4.
    """
    stop_index = len(datastream) - 14
    for index, value in enumerate(datastream):
        # Make sure our slice won't overrun the end of the datastream
        if index > stop_index:
            break
        if slice_is_start_of_message(datastream[index : index + 14]):
            return index + 14


if __name__ == "__main__":
    print("NB: Run this program from the root directory of the repository.\n")

    # Day of the AOC
    day = 6
    data_dir = f"data/{day:02}/"
    # file_name = f'{data_dir}day_{day:02}_test_01.txt'
    # file_name = f'{data_dir}day_{day:02}_test_02.txt'
    # file_name = f'{data_dir}day_{day:02}_test_03.txt'
    # file_name = f'{data_dir}day_{day:02}_test_04.txt'
    # file_name = f'{data_dir}day_{day:02}_test_05.txt'
    file_name = f'{data_dir}day_{day:02}_real.txt'

    # Define the example data file and the puzzle input data
    INPUT_DATA_PATH = Path.cwd() / f"./{file_name}"

    if not INPUT_DATA_PATH.is_file():
        raise FileNotFoundError(
            f"Error: File does not exist: {INPUT_DATA_PATH}"
        )

    with INPUT_DATA_PATH.open(mode="r", encoding="utf-8") as in_file_data:
        input_str = in_file_data.read().strip()

    print(f"Day {day:02} - Part 1:", part1(input_str))
    print(f"Day {day:02} - Part 2:", part2(input_str))
