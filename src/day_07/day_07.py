#!/usr/bin/env python3.11
"""Solution to Day 7 of AoC 2022."""

from collections import defaultdict
from pathlib import Path


def process_input(input_string):
    """From input string, return a list of commands and their output."""
    # Split on the $ symbol - this is used to signify a line is a command
    # All entries in the list will begin with the command that generated the output
    cmd_and_op = list(input_string.split("$"))

    # We are splitting on an unusual character, so filter the list to remove and empty strings
    cmd_and_op = list(filter(("").__ne__, cmd_and_op))

    # For each value in the list, clean up any trailing whitespace, then split into lines.
    # Command will be in the 0th element of list
    for idx, val in enumerate(cmd_and_op):
        tmp = val.strip().split("\n")

        # Create the object we will put back into the list
        out_dict = defaultdict(list)

        # Since the 0th element is always the command, split it so we can also see any arguments
        out_dict["command"] = tmp[0].split(" ")

        if len(tmp) > 1:
            # Move any remaining elements into a second list
            tmp2 = []
            for _, x in enumerate(tmp[1:]):
                tmp2.append(x.split(" "))
            out_dict["result"] = tmp2

        # Update the list we are processing, with the intention to return it
        cmd_and_op[idx] = out_dict

    # Return the list of lists
    return cmd_and_op


def process_filetree(commands_and_outputs):
    # Dict to store all the pertinent folder information
    # {
    #   "a" : 0
    # }
    dir_sizes = defaultdict(int)
    # dir_sizes = []

    stack = []

    # Populate the directory sizes
    for entry in commands_and_outputs:
        if entry["command"][0] == "cd":
            # If we are changing to the root directory, clear the stack
            if entry["command"][1] == "/":
                stack = ["/"]
                # stack = [""]
            # If we are leaving to the parent directory, remove the last element from the stack
            elif entry["command"][1] == "..":
                stack.pop()
            # If we are changing into a directory, add that to the stack
            else:
                stack.append(entry["command"][1] + "/")
        elif entry["command"][0] == "ls":
            for _, val in enumerate(entry["result"]):
                # if it tells us a directory is in this folder, we don't care
                if val[0] == "dir":
                    pass
                else:
                    dir_path = ""
                    for dir_section in stack:
                        dir_path = "".join([dir_path, dir_section])
                        dir_sizes[dir_path] += int(val[0])

    return dir_sizes


def part1(dir_sizes):
    """Return the sum of size of dirs, for all dirs smaller than 100000."""

    # Filter out all the directories smaller than 100000
    small_dirs = [size for size in dir_sizes.values() if 100000 >= size]

    # Return the sum of the small directories sizes
    return sum(small_dirs)


def part2(dir_sizes):
    """Return..."""
    total_space = 70000000
    desired_unused = 30000000

    sorted_dir_sizes = list(dir_sizes.values())
    sorted_dir_sizes.sort()

    current_unused = total_space - sorted_dir_sizes[-1]

    size_to_recover = desired_unused - current_unused

    small_dirs = [
        size for size in dir_sizes.values() if size >= size_to_recover
    ]
    small_dirs.sort()

    return small_dirs[0]


if __name__ == "__main__":
    print("NB: Run this program from the root directory of the repository.\n")

    # Day of the AOC
    day = 7
    data_dir = f"data/{day:02}/"
    # file_name = f'{data_dir}day_{day:02}_test.txt'
    file_name = f'{data_dir}day_{day:02}_real.txt'

    # Define the example data file and the puzzle input data
    INPUT_DATA_PATH = Path.cwd() / f"./{file_name}"

    if not INPUT_DATA_PATH.is_file():
        raise FileNotFoundError(
            f"Error: File does not exist: {INPUT_DATA_PATH}"
        )

    with INPUT_DATA_PATH.open(mode="r", encoding="utf-8") as in_file_data:
        input_str = in_file_data.read().strip()

    parsed_data = process_input(input_str)

    list_of_dir_sizes = process_filetree(parsed_data)

    print(f"Day {day:02} - Part 1:", part1(list_of_dir_sizes))
    print(f"Day {day:02} - Part 2:", part2(list_of_dir_sizes))
