#!/usr/bin/env python3.11
"""Solution to Day 9 of AoC 2022."""

import math
from collections import defaultdict
from pathlib import Path


def process_input(input_string):
    """From input string, return a list of commands and their output."""
    moves_list = input_string.strip().split("\n")
    for idx, val in enumerate(moves_list):
        moves_list[idx] = val.split(" ")
        moves_list[idx][1] = int(moves_list[idx][1])
    return moves_list


class Rope:
    """Class to hold a Rope game with a number of knots in the rope."""

    def __init__(self, num_knots):
        """Initialise the rope game."""
        self.num_knots = num_knots

        self.origin = (0, 0)

        self.visited = set()
        self.visited.add(self.origin)

        # Use a dict to store the positions of all the knots
        self.knots = defaultdict(dict)

        # For each knot, use it's index in the rope (0th is Head, Nth is Tail)
        # as the dictionary key. For each key store current x and y position.
        for knot in range(self.num_knots):
            self.knots[knot] = {"x": 0, "y": 0}

        # Initialise the Head as pointing at the very front knot of the Rope
        # Initialise the Tail as pointing at the very last knot of the Rope
        self.Head = self.knots[0]
        self.Tail = self.knots[self.num_knots - 1]

    def tail_adjacent_to_h(self):
        """Check if the current 'Tail' knot is adjacent to the current 'Head' knot."""
        x_difference = abs(self.Head["x"] - self.Tail["x"])
        y_difference = abs(self.Head["y"] - self.Tail["y"])

        if x_difference not in [0, 1]:
            return False
        elif y_difference not in [0, 1]:
            return False
        else:
            return True

    def move_t_next_to_h(self):
        """Relocate the current 'Tail' knot next to the current 'Head knot.

        If the head is ever two steps directly up, down, left, or right from the tail, the tail must also move one step in that direction so it remains close enough.
        Otherwise, if the head and tail aren't touching and aren't in the same row or column, the tail always moves one step diagonally to keep up.
        """
        x_diff = self.Head["x"] - self.Tail["x"]
        y_diff = self.Head["y"] - self.Tail["y"]

        x_modifier = -1 if x_diff < 0 else 1
        y_modifier = -1 if y_diff < 0 else 1

        # Not in the same row or column, so move diagonally
        if x_diff and y_diff:
            # Can't just add 1 because we need to potentially go in 4 directions
            # North-East, South-East, North-West, and South-West
            self.Tail["x"] += x_modifier * int(math.ceil(abs(x_diff / 2)))
            self.Tail["y"] += y_modifier * int(math.ceil(abs(y_diff / 2)))

        # If we are in the same column, only need to adjust Y
        elif x_diff:
            # positive X diff means H is above T
            # negative X diff means H is below T
            self.Tail["x"] += x_modifier * int(math.ceil(abs(x_diff / 2)))

        # If we are in the same row, only need to adjust X
        elif y_diff:
            # positive Y diff means H is Right of T
            # negative Y diff means H is Left of T
            self.Tail["y"] += y_modifier * int(math.ceil(abs(y_diff / 2)))

        else:
            raise ValueError(
                "x_diff and y_diff came back as 0", x_diff, y_diff
            )

    def process_step(self, action):
        """For the current move, process one move Up, Down, Left or Right for the head of the rope, then update any following knots affected."""
        self.Head = self.knots[0]

        if action == "R":
            self.Head["x"] += 1
        elif action == "L":
            self.Head["x"] -= 1
        elif action == "U":
            self.Head["y"] += 1
        elif action == "D":
            self.Head["y"] -= 1
        else:
            raise ValueError(
                "action contains movement we can't process:", action
            )

        for knot in range(self.num_knots - 1):
            self.Head = self.knots[knot]
            self.Tail = self.knots[knot + 1]
            if not self.tail_adjacent_to_h():
                self.move_t_next_to_h()

            # if knot == self.num_knots - 1:
            #     t_location = tuple([self.Tail["y"], self.Tail["x"]])
            #     self.visited.add(t_location)

    def process_move(self, vector):
        """Process a single line from the input file."""
        for i in range(vector[1]):
            self.process_step(vector[0])
            (self.knots)

            t_location = tuple([self.Tail["y"], self.Tail["x"]])
            self.visited.add(t_location)

    def process_moves(self, moves_list):
        """Run through all the moves in the input file."""
        for _, move in enumerate(moves_list):
            self.process_move(move)


def part1(list_of_moves):
    rope_game = Rope(2)
    rope_game.process_moves(list_of_moves)

    # Return the number of locations visited at least once
    return len(rope_game.visited)


def part2(list_of_moves):
    rope_game = Rope(10)
    rope_game.process_moves(list_of_moves)

    # Return the number of locations visited at least once
    return len(rope_game.visited)


if __name__ == "__main__":
    print("NB: Run this program from the root directory of the repository.\n")

    # Day of the AOC
    day = 9
    data_dir = f"data/{day:02}/"
    # file_name = f'{data_dir}day_{day:02}_test_01.txt'
    # file_name = f'{data_dir}day_{day:02}_test_02.txt'
    file_name = f'{data_dir}day_{day:02}_real.txt'

    # Define the example data file and the puzzle input data
    INPUT_DATA_PATH = Path.cwd() / f"./{file_name}"

    if not INPUT_DATA_PATH.is_file():
        raise FileNotFoundError(
            f"Error: File does not exist: {INPUT_DATA_PATH}"
        )

    with INPUT_DATA_PATH.open(mode="r", encoding="utf-8") as in_file_data:
        input_str = in_file_data.read()

    parsed_data = process_input(input_str)

    print(f"Day {day:02} - Part 1:", part1(parsed_data))
    print(f"Day {day:02} - Part 2:", part2(parsed_data))
