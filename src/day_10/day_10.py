#!/usr/bin/env python3.11
"""Solution to Day 10 of AoC 2022."""


from pathlib import Path


def process_input(input_string):
    """From input string, return a list of commands and their output."""
    moves_list = input_string.strip().split("\n")
    for idx, val in enumerate(moves_list):
        moves_list[idx] = val.split(" ")
        if len(moves_list[idx]) > 1:
            moves_list[idx][1] = int(moves_list[idx][1])
    return moves_list


class VirtualMachine:
    """Object to hold the virtual machine and it's methods."""

    def __init__(self, instructions_list: list):
        """
        Initialise the object with variables that will be used later.

        Thing: Description
        """
        # Somewhere to store the commands input
        self.programme = instructions_list

        # Length of the programme
        self.programme_len = len(self.programme)

        # Programme counter to point to where we are in the programme
        self.pc = 0

        # Cycle counter. Required to calculate answer to part 1.
        self.cycle = 0

        # The register X affected by addx
        self.x = 1

        # Somewhere to store signal_strength history
        self.signal_strength = []

        # Output buffer here represents the 40x6 line display
        self.outbuf = [" "] * 240

    def _update_signal_strength(self):
        """Store a new signal strength if we are at a valid cycle to do so."""
        if self.cycle in [20, 60, 100, 140, 180, 220]:
            self.signal_strength.append(self.cycle * self.x)

    def _update_outbuf(self):
        """Store characters to print to screen later.

        The screen is a 40 x 6 grid, stored as a 240 character buffer.
        self.x gives us the horizontal position of a sprite.
        (self.cycle - 1) % 40 gives us our current position along a row.
        If the sprite and current position overlap, an output is generated.
        """
        if ((self.cycle - 1) % 40) in [self.x - 1, self.x, self.x + 1]:
            self.outbuf[self.cycle - 1] = "#"

    def _noop(self):
        """No Operation, takes 1 cycle to execute."""
        self.cycle += 1
        self._update_signal_strength()
        self._update_outbuf()
        self.pc += 1

    def _addx(self):
        """Update X, takes 2 cycles to execute."""
        self.cycle += 1
        self._update_signal_strength()
        self._update_outbuf()

        self.cycle += 1
        self._update_signal_strength()
        self._update_outbuf()
        self.x += self.programme[self.pc][1]
        self.pc += 1

    def run_programme(self):
        """Run the programme from the parsed commands."""
        for _ in range(self.programme_len):
            if self.programme[self.pc][0] == "noop":
                self._noop()
            elif self.programme[self.pc][0] == "addx":
                self._addx()
            else:
                raise ValueError(
                    "Not sure what to do with this command:",
                    self.programme[self.pc][0],
                )


def part1(instructions):
    """Return the sum of the signal strengths.

    Signal strenghts are recorded in the 20th, 60th, 100th, 140th, 180th and 200th cycle.
    """
    vm = VirtualMachine(instructions)
    vm.run_programme()
    return sum(vm.signal_strength)


def part2(instructions):
    """Return a formatted output buffer of strings.

    Printing each line in order to screen will reveal a code.
    """
    vm = VirtualMachine(instructions)
    vm.run_programme()
    formatted_outbuf = []
    for i in range(6):
        i_min = i * 40
        i_max = (i + 1) * 40
        formatted_outbuf.append(vm.outbuf[i_min:i_max])

    for idx, val in enumerate(formatted_outbuf):
        formatted_outbuf[idx] = "".join(val)
        print(formatted_outbuf[idx])

    return formatted_outbuf


if __name__ == "__main__":
    print("NB: Run this program from the root directory of the repository.\n")

    # Day of the AOC
    day = 10
    data_dir = f"data/{day:02}/"
    # file_name = f'{data_dir}day_{day:02}_test_01.txt'
    # file_name = f'{data_dir}day_{day:02}_test_02.txt'
    file_name = f'{data_dir}day_{day:02}_real.txt'

    # Define the example data file and the puzzle input data
    INPUT_DATA_PATH = Path.cwd() / f"./{file_name}"

    if not INPUT_DATA_PATH.is_file():
        raise FileNotFoundError(
            f"Error: File does not exist: {INPUT_DATA_PATH}"
        )

    with INPUT_DATA_PATH.open(mode="r", encoding="utf-8") as in_file_data:
        input_str = in_file_data.read()

    parsed_data = process_input(input_str)

    print(f"Day {day:02} - Part 1:", part1(parsed_data))
    print(f"Day {day:02} - Part 2:")
    part2(parsed_data)
