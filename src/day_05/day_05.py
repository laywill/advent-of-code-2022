#!/usr/bin/env python3.11
"""Solution to Day 5 of AoC 2022."""

from collections import defaultdict
from pathlib import Path


class stack_solver:
    """Class to hold data structures and methods for stacks and move lists."""

    def __init__(self, input_string):
        """Initialise the stacks and moves."""
        self.stacks = None

        self.moves = None

        self.process_input(input_string)

    def process_input(self, input_string):
        """From input string, return a tuple of data structures, initial stacks and moves."""
        # Split into the top half of the input and the bottom half
        # Top half is the initial Stacks
        # Bottom half is the list of moves between stacks
        stacks_and_moves = input_string.split("\n\n")

        # Need to preserve whitespace in stacks, but strip from moves
        stacks_and_moves[1] = stacks_and_moves[1].strip()

        # Split the two strings into lists of lines
        for idx, text in enumerate(stacks_and_moves):
            stacks_and_moves[idx] = text.split("\n")

        # Process the stacks
        self.stacks = self.process_stacks_input(stacks_and_moves[0])

        # Process the moves list
        self.moves = self.process_moves_input(stacks_and_moves[1])

        return tuple([self.stacks, self.moves])

    def process_stacks_input(self, stacks_list):
        """Convert the raw stack strings into a programmatic data structure."""
        # Find the maximum and min column numbers
        max_col = int(max(stacks_list[-1]))
        min_col = 1

        # Add 1 to max to make sure we generate correct range of keys
        keys = list(map(int, range(min_col, max_col + 1)))

        # Initialise a dict to store stacks in, with empty lists
        # stacks = dict.fromkeys(keys, [])
        stacks = defaultdict(list)

        # Calculate the string indexes required for each stack
        stacks_to_col = dict.fromkeys(keys)
        for col in range(1, max_col + 1):
            stacks_to_col[col] = 1 + ((col - 1) * 4)

        # Reverse the list so stack numbers are in idx[0]
        stacks_list.reverse()

        # Load the stacks up
        for idx, line in enumerate(stacks_list):
            # Don't process the stack label line
            if idx == 0:
                pass
            else:
                for key in stacks_to_col:
                    value = line[stacks_to_col[key]]

                    if value == " ":
                        pass
                    else:
                        stacks[key] += value
        return stacks

    def process_moves_input(self, moves_list):
        """Convert the raw list of moves into a programmatic data structure."""
        # List of moves to populate
        move_commands = []

        for idx, text in enumerate(moves_list):
            # Generate a list of strings
            keywords = text.split(" ")

            # Create a dict summarizing the keyword information
            command = {
                "number": int(keywords[1]),
                "from": int(keywords[3]),
                "to": int(keywords[5]),
            }

            # Add it to the list of moves
            move_commands.append(command)
        return tuple(move_commands)

    def do_move(self, this_move, move_all_at_once):
        """Update the stacks, applying one move.

        can_move_all_at_once allows the crane to pick up a deep vertical stack off the top of a stack.
        If it is false, it has to move the elements one at a time from one stack to the other,
        this reverses their order as they are moved.
        """
        _number = this_move["number"]
        _from = this_move["from"]
        _to = this_move["to"]
        temp = self.stacks[_from][-_number:]
        if not move_all_at_once:
            temp.reverse()
        self.stacks[_from] = self.stacks[_from][:-_number]
        self.stacks[_to] += temp

        return self.stacks

    def do_all_moves(self, can_move_all_at_once):
        """Update the stacks, applying all moves.

        can_move_all_at_once allows the crane to pick up a deep vertical stack off the top of a stack.
        If it is false, it has to move the elements one at a time from one stack to the other,
        this reverses their order as they are moved.
        """
        for _, move in enumerate(self.moves):
            self.do_move(move, can_move_all_at_once)

        return self.stacks

    def top_of_stacks(self):
        """Return list of strings, each element from top of each stack."""
        out = []
        for key in self.stacks:
            out.append(self.stacks[key][-1])

        return out


def part1(solver):
    """Return the string made from joining top of each stack chars."""
    solver.do_all_moves(can_move_all_at_once=False)

    return "".join(solver.top_of_stacks())


def part2(solver):
    """Return the string made from joining top of each stack chars."""
    solver.do_all_moves(can_move_all_at_once=True)

    return "".join(solver.top_of_stacks())


if __name__ == "__main__":
    print("NB: Run this program from the root directory of the repository.\n")

    # Day of the AOC
    day = 5
    data_dir = f"data/{day:02}/"
    # file_name = f'{data_dir}day_{day:02}_test.txt'
    file_name = f'{data_dir}day_{day:02}_real.txt'

    # Define the example data file and the puzzle input data
    INPUT_DATA_PATH = Path.cwd() / f"./{file_name}"

    if not INPUT_DATA_PATH.is_file():
        raise FileNotFoundError(
            f"Error: File does not exist: {INPUT_DATA_PATH}"
        )

    with INPUT_DATA_PATH.open(mode="r", encoding="utf-8") as in_file_data:
        input_str = in_file_data.read()

    solver1 = stack_solver(input_str)
    solver2 = stack_solver(input_str)

    print(f"Day {day:02} - Part 1:", part1(solver1))
    print(f"Day {day:02} - Part 2:", part2(solver2))
