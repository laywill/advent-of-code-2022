#!/usr/bin/env python3.11
"""Solution to Day 3 of AoC 2022."""

import string
from pathlib import Path

# List of strings to use as a lookup
alphabet = list(string.ascii_letters)


def process_input(input_str):
    """From input string, return a list of rounds, each round is a tuple of moves."""
    list_of_bags = list(input_str.split("\n"))
    return list_of_bags


def part1(bags):
    """Return the sum of priorities of item that appears in both pouches of each bag.

    Position in alphabet (1 indexed) is the priority.
    """
    bags_with_pouches = []
    for _, bag_str in enumerate(bags):
        split_idx = int(len(bag_str) / 2)
        compartment1 = bag_str[0:split_idx]
        compartment2 = bag_str[split_idx:]
        bags_with_pouches.append([compartment1, compartment2])

    total = 0
    for _, bag in enumerate(bags_with_pouches):
        set1 = set(list(bag[0]))
        set2 = set(list(bag[1]))
        unique_char = list(set1.intersection(set2))
        if len(unique_char) != 1:
            raise ValueError(
                "More than one unique character was found in a bag.",
                unique_char,
            )
        index = alphabet.index(unique_char[0])
        total += index + 1
    return total


def part2(bags):
    """Return the sum of priorities of he item that is common in each group of three bags.

    Position in alphabet (1 indexed) is the priority.
    """
    total = 0

    for bag1, bag2, bag3 in zip(*[iter(bags)] * 3):
        set1 = set(list(bag1))
        set2 = set(list(bag2))
        set3 = set(list(bag3))
        unique_char = list(set1 & set2 & set3)
        if len(unique_char) != 1:
            raise ValueError(
                "More than one unique character was found in a bag.",
                unique_char,
            )
        index = alphabet.index(unique_char[0])
        total += index + 1
    return total


if __name__ == "__main__":
    print("NB: Run this program from the root directory of the repository.\n")

    # Day of the AOC
    day = 3
    data_dir = f"data/{day:02}/"
    # file_name = f'{data_dir}day_{day:02}_test.txt'
    file_name = f'{data_dir}day_{day:02}_real.txt'

    # Define the example data file and the puzzle input data
    INPUT_DATA_PATH = Path.cwd() / f"./{file_name}"

    if not INPUT_DATA_PATH.is_file():
        raise FileNotFoundError(
            f"Error: File does not exist: {INPUT_DATA_PATH}"
        )

    with INPUT_DATA_PATH.open(mode="r", encoding="utf-8") as in_file_data:
        input_str = in_file_data.read().strip()
    rucksacks = process_input(input_str)

    print(f"Day {day:02} - Part 1:", part1(rucksacks))
    print(f"Day {day:02} - Part 2:", part2(rucksacks))
