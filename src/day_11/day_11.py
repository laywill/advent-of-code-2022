#!/usr/bin/env python3.11
"""Solution to Day 11 of AoC 2022."""

import math
from collections import defaultdict
from pathlib import Path


def process_input(input_string):
    """From input string, return a list of commands and their output."""
    monkey_strings = input_string.strip().split("\n\n")
    for idx, monkey in enumerate(monkey_strings):
        # Get just the first line as one object, and all remaining as another
        monkey_num, monkey_data = monkey.strip().split("\n", maxsplit=1)

        # Strip out all the text that isn't integers in the monkey number
        monkey_num = int(monkey_num.strip("MONKEYmonkey :"))

        # Split into lines for the data
        monkey_data = monkey_data.split("\n")

        monkey_strings[idx] = [monkey_num, monkey_data]
    return monkey_strings


class Monkey:
    """Class to hold a Monkey."""

    def __init__(
        self, monkey_num: int, monkey_input: list, worry_divider_enabled: bool
    ):
        """Initialise the rope game."""

        self.monkey_number = monkey_num

        # a list of integer items the monkey is holding
        self.items = self._create_initial_items(monkey_input[0])

        # The operation this monkey performs
        self.operation = self._define_operation(monkey_input[1])

        # All monkeys have a test in the form:
        # Test: divisible by 13
        # We only need store the integer
        self.test_int = self._get_test_int(monkey_input[2])

        # The test throws to one monkey if true, and another if false
        # Get those values.
        self.test_true_val = self._get_test_int(monkey_input[3])
        self.test_false_val = self._get_test_int(monkey_input[4])

        # Track total number of items inspected over time
        self.num_items_inspected = 0

        self.divide_worry_level = worry_divider_enabled

        # Lowest common Multiple of the dividers, used for part 2
        self.mod_lcm = None

    def _create_initial_items(self, items: str) -> list:
        """Populate the list of items with int values"""
        # Remove any characters that are present in the preamble and any trailing whitespace
        # Then split on the comma separated values to create a list of strings
        numbers = items.strip("\tStarting items: \n").split(",")

        # Cast that to a list of ints and store.
        return list(map(int, numbers))

    def _define_operation(self, operation_text: str) -> dict:
        """Parse the text description to get an algorithm processor argument.

        For the purpose of this puzzle, the operation is always a + or *

        Examples can take the form:
        Operation: new = old * 19
        Operation: new = old + 6
        Operation: new = old * old
        """
        opr = ""
        if "+" in operation_text:
            opr = "+"
        elif "*" in operation_text:
            opr = "*"
        else:
            raise ValueError(
                "This monkey does an operation that isn't + or *!:",
                operation_text,
            )

        # Get the constituent parts, so we can work out what gets done to the old number
        parts = operation_text.strip().split(" ")

        if parts[-1] == "old":
            val = parts[-1]
        else:
            val = int(parts[-1])

        return {"opr": opr, "val": val}

    def _get_test_int(self, test_val: str) -> None:
        """Store the test integer to be used later in tests

        All monkeys have a test in the form:
        Test: divisible by 13
        We only need store the integer
        """
        parts = test_val.strip().split(" ")
        return int(parts[-1])

    def _monkey_test(self, val_to_test: int) -> bool:
        """Do the test this monkey does.

        All monkeys have a test in the form:
        Test: divisible by 13
        Return True or False
        """
        # Deliberately back to front here than what you would expect
        # Modulo (%) returns an integer if there is remainder, but puzzle expression
        # Asks if DIVISIBLE by n exactly. So can't just return result.
        return_bool = False if val_to_test % self.test_int else True
        return return_bool

    def inspect_and_throw(self):
        return_dict = defaultdict(list)

        # If the list is empty, return an empty dict and save yourself the hassle
        if not self.items:
            return return_dict

        # Track the total number of items this monkey inspects
        self.num_items_inspected += len(self.items)

        # This monkey has items to inspect.
        for idx, item in enumerate(self.items):
            # This could probably be simplified by maintaining a pointer of some sort
            # avoiding a lookup every iteration, since it won't change.
            if self.operation["val"] == "old":
                opr_val = item
            else:
                opr_val = self.operation["val"]

            # Convert the operator to an action
            if self.operation["opr"] == "*":
                new_val = item * opr_val
            elif self.operation["opr"] == "+":
                new_val = item + opr_val
            else:
                raise ValueError(
                    "The value in self.operation['opr'] is unexpected:",
                    self.operation["opr"],
                )

            if self.mod_lcm:
                new_val = new_val % self.mod_lcm

            # Your relief that the monkey's inspection didn't damage the item causes your worry
            # level to be divided by three and rounded down to the nearest integer.
            if self.divide_worry_level:
                new_val = int(math.floor(new_val / 3))

            if self._monkey_test(new_val):
                return_dict[self.test_true_val].append(new_val)
            else:
                return_dict[self.test_false_val].append(new_val)

        # Assume that all items are thrown, none are retained
        self.items = []

        return return_dict


class MonkeyBusiness:
    def __init__(self, monkey_input_data, div_by_3: bool = False):
        self.enable_div_by_3 = div_by_3
        self.monkeys = self._create_monkeys(monkey_input_data)
        self.num_monkeys = len(monkey_input_data)

        self.num_rounds_played = 0

        self.enable_mod_lcm()

    def _create_monkeys(self, monkey_data):
        """Used to initialise Monkey data."""
        monkeys = defaultdict(Monkey)

        for _, monkey in enumerate(monkey_data):
            monkeys[monkey[0]] = Monkey(
                monkey_num=monkey[0],
                monkey_input=monkey[1],
                worry_divider_enabled=self.enable_div_by_3,
            )

        return monkeys

    def play_round(self):
        self.num_rounds_played += 1
        for key in self.monkeys.keys():
            thrown_items = self.monkeys[key].inspect_and_throw()

            # thrown items is a dict, where keys are monkey to send things to
            for throw_key in thrown_items.keys():
                # Use extend to concatenate list items onto the end
                self.monkeys[throw_key].items.extend(thrown_items[throw_key])

    def play_rounds(self, num_turns: int):
        for turn in range(num_turns):
            self.play_round()

    def get_monkey_inspection_counts(self) -> list[int]:
        counts = []
        for key in self.monkeys.keys():
            counts.append(self.monkeys[key].num_items_inspected)

        return counts

    def _get_lowest_common_denominator(self) -> int:
        dividers = []
        for key in self.monkeys.keys():
            dividers.append(self.monkeys[key].test_int)

        lcm = dividers[0]
        for i in range(1, len(dividers)):
            lcm = math.lcm(lcm, dividers[i])
        return lcm

    def enable_mod_lcm(self):
        monkeys_lcm = self._get_lowest_common_denominator()
        for key in self.monkeys.keys():
            self.monkeys[key].mod_lcm = monkeys_lcm


def part1(input_monkey_data) -> int:
    mb = MonkeyBusiness(input_monkey_data, True)

    mb.play_rounds(20)

    # The level of monkey business is the number of items inspected by two busiest monkeys, multiplies
    total_inspections = mb.get_monkey_inspection_counts()
    total_inspections.sort()
    return total_inspections[-2] * total_inspections[-1]


def part2(input_monkey_data) -> int:
    mb = MonkeyBusiness(input_monkey_data, False)

    mb.enable_mod_lcm()

    mb.play_rounds(10000)

    # The level of monkey business is the number of items inspected by two busiest monkeys, multiplies
    total_inspections = mb.get_monkey_inspection_counts()
    total_inspections.sort()
    return total_inspections[-2] * total_inspections[-1]


if __name__ == "__main__":
    print("NB: Run this program from the root directory of the repository.\n")

    # Day of the AOC
    day = 11
    data_dir = f"data/{day:02}/"
    # file_name = f'{data_dir}day_{day:02}_test.txt'
    file_name = f'{data_dir}day_{day:02}_real.txt'

    # Define the example data file and the puzzle input data
    INPUT_DATA_PATH = Path.cwd() / f"./{file_name}"

    if not INPUT_DATA_PATH.is_file():
        raise FileNotFoundError(
            f"Error: File does not exist: {INPUT_DATA_PATH}"
        )

    with INPUT_DATA_PATH.open(mode="r", encoding="utf-8") as in_file_data:
        input_str = in_file_data.read()

    parsed_data = process_input(input_str)

    print(f"Day {day:02} - Part 1:", part1(parsed_data))
    print(f"Day {day:02} - Part 2:", part2(parsed_data))
