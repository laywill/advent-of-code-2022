#!/usr/bin/env python3.11
"""Solution to Day 14 of AoC 2022."""

from collections import defaultdict
from pathlib import Path

import numpy as np
import pandas as pd


def process_input(input_string: str):
    """Return a list of ... stuff"""
    input_lines = input_string.strip().split("\n")

    for idx, val in enumerate(input_lines):
        input_lines[idx] = input_lines[idx].strip().split(" -> ")

        # Now break a line down into coordinate pairs
        coords_list = []
        for i, v in enumerate(input_lines[idx]):
            coords = tuple(map(int, v.split(",")))
            coords_list.append({"x": coords[0], "y": coords[1]})
        input_lines[idx] = tuple(coords_list)

    return input_lines


class Cave:
    def __init__(self, input_coords, enable_floor=False):
        self.rocks = input_coords

        self.floor = enable_floor

        # We want to know what the leftmost and rightmost column is really.
        self.cave_min_col = 500
        self.cave_max_col = 500
        self.cave_top_row = 0
        self.cave_bot_row = 0
        self._get_size_required()

        # Define the characters representing various things
        self.rock_char = "#"
        self.sand_char = "O"
        self.pour_char = "+"

        # A pandas dataframe, which contains only one cell.
        # Located at col 500, row 0, containing a "+" representing where sand enters the cave.
        self.cave = pd.DataFrame(self._gen_dict())
        self.cave[500][0] = self.pour_char

        self._add_rocks_to_cave()

        # The coordinate at which sand enters the cave.
        self.sand_pour_coord = {"x": 500, "y": 0}

        self.sand_count = 0

        self.sand_overflow = False

        self.search_stack = []

    def _get_size_required(self):
        """Find out how big x and y need to be."""
        for rock in self.rocks:
            for node in rock:
                if node["x"] > self.cave_max_col:
                    self.cave_max_col = node["x"]
                elif node["x"] < self.cave_min_col:
                    self.cave_min_col = node["x"]

                if node["y"] > self.cave_bot_row:
                    self.cave_bot_row = node["y"]

    def _gen_empty_column(self):
        if not self.floor:
            # Anything not within bounds of the array has fallen into the void
            # The array is the size of the rocks we can see
            default_column = ["."] * (self.cave_bot_row + 1)
        else:
            # There is a floor, 2 rows below the lowest rock in our input.
            # Add a row of gap, then a row of rock.
            default_column = ["."] * (self.cave_bot_row + 2)
            default_column.append(self.rock_char)
        return default_column

    def _gen_dict(self):
        """Generate a dict that can be used to populate dataframe."""
        generator_dict = defaultdict(list)
        col_val = self._gen_empty_column()
        for i in range(self.cave_min_col, self.cave_max_col + 1):
            generator_dict[i] = col_val
        return generator_dict

    def _add_rocks_to_cave(self):
        for rock in self.rocks:
            for idx in range(len(rock) - 1):
                x_start = min(rock[idx]["x"], rock[idx + 1]["x"])
                x_end = max(rock[idx]["x"], rock[idx + 1]["x"])
                y_start = min(rock[idx]["y"], rock[idx + 1]["y"])
                y_end = max(rock[idx]["y"], rock[idx + 1]["y"])

                for x in range(x_start, x_end + 1):
                    for y in range(y_start, y_end + 1):
                        self.cave[x][y] = self.rock_char

    def count_sand(self):
        # Count number of zeros in all columns of Dataframe
        # count = 0
        # # for column_name in self.cave.columns:
        # #     column = self.cave[column_name]
        # #     # Get the count of Zeros in column
        # #     count += (column == self.sand_char).sum()
        count_arr = self.cave.to_numpy()
        # print(count_arr)
        count = len(np.where(count_arr == self.sand_char)[0])
        # print(count)
        self.sand_count = count
        return count

    def _check_down_blocked(self, location):
        col = location["x"]
        row = location["y"] + 1
        return self._check_blocked(col, row)

    def check_down_and_left_blocked(self, location):
        col = location["x"] - 1
        row = location["y"] + 1
        return self._check_blocked(col, row)

    def check_down_and_right_blocked(self, location):
        col = location["x"] + 1
        row = location["y"] + 1
        return self._check_blocked(col, row)

    def _check_blocked(self, x, y):
        if not self.floor:
            try:
                if self.cave[x][y].upper() == self.sand_char:
                    return True
                elif self.cave[x][y] == self.rock_char:
                    return True
                else:
                    return False
            except KeyError:
                self.sand_overflow = True
        else:
            if x not in self.cave.columns:
                # self.cave[x] = self._gen_empty_column()
                new_col = pd.DataFrame({x: self._gen_empty_column()})
                if x < self.cave_min_col:
                    self.cave_min_col -= 1
                    self.cave = pd.concat([new_col, self.cave], axis=1)
                else:
                    self.cave_max_col += 1
                    self.cave = pd.concat([self.cave, new_col], axis=1)

            if self.cave[x][y].upper() == self.sand_char:
                return True
            elif self.cave[x][y] == self.rock_char:
                return True
            else:
                return False

    def _cache_location(self, current_location):
        # Cache the location we are at to make future searches faster
        self.search_stack.append(dict(current_location))

    def _place_one_sand(self):
        # loc = dict(self.sand_pour_coord)

        # Save time by starting at the last location we didn't place sand
        if not self.search_stack:
            loc = dict(self.sand_pour_coord)
        else:
            loc = self.search_stack.pop()

        # print("self.search_stack", self.search_stack)
        # print(self.cave)
        # print(loc)

        while not self.sand_overflow:
            if self._check_down_blocked(loc):
                if self.check_down_and_left_blocked(loc):
                    if self.check_down_and_right_blocked(loc):
                        self.cave[loc["x"]][loc["y"]] = self.sand_char
                        # break
                        if self.search_stack:
                            self.search_stack.pop()
                        break
                    else:
                        loc["x"] += 1
                        loc["y"] += 1
                        continue
                else:
                    loc["x"] -= 1
                    loc["y"] += 1
                    continue
            else:
                loc["y"] += 1
                continue

        # self.cave[loc["x"]][loc["y"]] = self.sand_char
        if loc == self.sand_pour_coord:
            self.sand_overflow = True

    def pour_sand(self):
        sand = 0
        while True:
            sand += 1
            self._place_one_sand()
            if self.count_sand() != sand:
                print(
                    "Counted",
                    self.sand_count,
                    "but expected",
                    sand,
                    "tiles filled.",
                )
                break
            if self.sand_overflow:
                print("Sand overflow")
                break


def part1(input_data):
    puzzle = Cave(input_data)

    day = 14
    out_dir = f"output/{day:02}/"
    csv_file_name = f'{out_dir}day_{day:02}_empty_cave.csv'
    # Define the example data file and the puzzle input data
    OUTPUT_CSV_PATH = Path.cwd() / f"./{csv_file_name}"
    OUTPUT_CSV_PATH.parent.mkdir(parents=True, exist_ok=True)
    with OUTPUT_CSV_PATH.open(mode="w", encoding="utf-8") as out_file_data:
        puzzle.cave.to_csv(out_file_data)

    puzzle.pour_sand()

    csv_file_name = f'{out_dir}day_{day:02}_filled_cave_part_1.csv'
    # Define the example data file and the puzzle input data
    OUTPUT_CSV_PATH = Path.cwd() / f"./{csv_file_name}"
    OUTPUT_CSV_PATH.parent.mkdir(parents=True, exist_ok=True)
    with OUTPUT_CSV_PATH.open(mode="w", encoding="utf-8") as out_file_data:
        puzzle.cave.to_csv(out_file_data)

    return puzzle.sand_count


def part2(input_data):
    puzzle = Cave(input_data, enable_floor=True)

    puzzle.pour_sand()

    day = 14
    out_dir = f"output/{day:02}/"
    csv_file_name = f'{out_dir}day_{day:02}_filled_cave_part_2.csv'
    # Define the example data file and the puzzle input data
    OUTPUT_CSV_PATH = Path.cwd() / f"./{csv_file_name}"
    OUTPUT_CSV_PATH.parent.mkdir(parents=True, exist_ok=True)
    with OUTPUT_CSV_PATH.open(mode="w", encoding="utf-8") as out_file_data:
        puzzle.cave.to_csv(out_file_data)

    return puzzle.sand_count


def main():
    print("NB: Run this program from the root directory of the repository.\n")

    # Day of the AOC
    day = 14
    data_dir = f"data/{day:02}/"
    # file_name = f'{data_dir}day_{day:02}_test.txt'
    file_name = f'{data_dir}day_{day:02}_real.txt'

    # Define the example data file and the puzzle input data
    INPUT_DATA_PATH = Path.cwd() / f"./{file_name}"

    if not INPUT_DATA_PATH.is_file():
        raise FileNotFoundError(
            f"Error: File does not exist: {INPUT_DATA_PATH}"
        )

    with INPUT_DATA_PATH.open(mode="r", encoding="utf-8") as in_file_data:
        input_str = in_file_data.read()

    parsed_data = process_input(input_str)

    print(f"Day {day:02} - Part 1:", part1(parsed_data))
    print(f"Day {day:02} - Part 2:", part2(parsed_data))


if __name__ == "__main__":
    main()
