#!/usr/bin/env python3.11
"""Solution to Day 15 of AoC 2022."""

import ast
import queue
from collections import defaultdict
from cProfile import Profile
from pathlib import Path
from pstats import SortKey, Stats


class Sensor:
    def __init__(
        self, sensor_x: int, sensor_y: int, beacon_x: int, beacon_y: int
    ):
        """Initialise control logic."""
        # Location of this sensor on the map
        self.location = {"row": sensor_y, "col": sensor_x}

        # Location of the closest beacon on the amp
        self.closest_beacon = {"row": beacon_y, "col": beacon_x}

        # Manhattan Distance from Sensor to closest beacon
        self.manhattan_distance = self._manhattan(
            [self.location["row"], self.location["col"]],
            [self.closest_beacon["row"], self.closest_beacon["col"]],
        )

    def _manhattan(self, A: list, B: list):
        """Return integer manhattan distance between two 2D points."""
        return sum(abs(val1 - val2) for val1, val2 in zip(A, B))


class Tunnel_Map:
    def __init__(self, puzzle_input):
        self.map_size = {
            "row_top": 0,
            "row_bottom": 0,
            "col_left": 0,
            "col_right": 0,
        }

        # Define the characters representing various things
        self.sensor_char = "S"
        self.beacon_char = "B"
        self.covered_char = "#"
        self.empty_char = "."

        # List of all the sensors
        self.sensors = []
        self._process_input(puzzle_input)

        # A pandas dataframe, which contains only one cell.
        # Located at col 0, row 0, containing a "." .
        # self.map = pd.DataFrame(
        #     data=self._gen_dict(),
        #     index=list(
        #         range(
        #             self.map_size["row_top"], self.map_size["row_bottom"] + 1
        #         )
        #     ),
        # )

        # self._add_beacons_and_sensors_to_map()

    def map_width(self):
        """Return the width of the map as an integer number of columns."""
        return 1 + self.map_size["col_right"] - self.map_size["col_left"]

    def map_height(self):
        """Return the height of the map as an integer number of rows."""
        return 1 + self.map_size["row_bottom"] - self.map_size["row_top"]

    def _gen_empty_column(self):
        default_column = ["."] * (
            self.map_size["row_bottom"] - self.map_size["row_top"] + 1
        )
        return default_column

    def _process_input(self, puzzle_input: str):
        list_of_sensors = self._string_to_list_of_strings(puzzle_input)

        for line in list_of_sensors:
            line = line.split(" ")

            sensor_x = int(line[2][2:].strip(","))
            sensor_y = int(line[3][2:].strip(":"))

            beacon_x = int(line[8][2:].strip(","))
            beacon_y = int(line[9][2:])

            instance = Sensor(sensor_x, sensor_y, beacon_x, beacon_y)

            min_row = instance.location["row"] - instance.manhattan_distance
            max_row = instance.location["row"] + instance.manhattan_distance

            min_col = instance.location["col"] - instance.manhattan_distance
            max_col = instance.location["col"] + instance.manhattan_distance

            self._update_map_edges(min_row, min_col)
            self._update_map_edges(max_row, max_col)

            self.sensors.append(instance)

    def _string_to_list_of_strings(self, multiline_string: str):
        """From input string, return a list of, for each elf, list of snack calorie values."""
        return list(multiline_string.strip().split("\n"))

    def _update_map_edges(self, new_x: int, new_y: int):
        if new_y < self.map_size["row_top"]:
            self.map_size["row_top"] = new_y
        elif new_y > self.map_size["row_bottom"]:
            self.map_size["row_bottom"] = new_y

        if new_x < self.map_size["col_left"]:
            self.map_size["col_left"] = new_x
        elif new_x > self.map_size["col_right"]:
            self.map_size["col_right"] = new_x

        return None


def _manhattan(A: list, B: list):
    """Return integer manhattan distance between two 2D points."""
    return sum(abs(val1 - val2) for val1, val2 in zip(A, B))


def _strings_in_string(string_list, test_string):
    """
    Compare a list of strings to a target string.
    Return a list of any of the strings that appear in the target string.
    """
    return [e for e in string_list if e in test_string]


def _list_intersection(lst1, lst2):
    """
    Return the intersection of two lists.
    """
    return [value for value in lst1 if value in lst2]


def _check_cell(
    tunnel_map_object, cell_under_test, row_under_test, col_under_test
):
    checked_cell = cell_under_test
    for sensor in tunnel_map_object.sensors:
        # Test if this cell is already marked as within range of a sensor
        if checked_cell == tunnel_map_object.covered_char:
            # Already scanned!
            break

        # Test if a sensor is already in this cell
        if sensor.location["row"] == row_under_test:
            if sensor.location["col"] == col_under_test:
                # A Sensor is in this cell!
                checked_cell = tunnel_map_object.sensor_char

        # Test if a beacon is already in this cell
        if sensor.closest_beacon["row"] == row_under_test:
            if sensor.closest_beacon["col"] == col_under_test:
                # A Beacon is in this cell!
                checked_cell = tunnel_map_object.beacon_char
                break

        # We aren't a beacon, sensor or already in range, so check if we are in range of this sensor.
        cell_to_sensor_manhattan = _manhattan(
            [sensor.location["row"], sensor.location["col"]],
            [row_under_test, col_under_test],
        )
        if cell_to_sensor_manhattan <= sensor.manhattan_distance:
            chars = ["S", "B"]
            if not _list_intersection(list(checked_cell), chars):
                checked_cell = tunnel_map_object.covered_char
                break

    return checked_cell


def part1(input_data, test_row) -> int:
    # Create a tunnel map object with sensors and beacons populated
    tm = Tunnel_Map(input_data)

    row_under_test = ["."] * tm.map_width()

    for idx, col in enumerate(
        list(range(tm.map_size["col_left"], tm.map_size["col_right"] + 1))
    ):
        cell = row_under_test[idx]
        row_under_test[idx] = _check_cell(tm, cell, test_row, col)

    return row_under_test.count("#")


def part2(input_data, search_range_max) -> int:
    """
    There is only one possible undetected beacon that must locate just one step outside of sensors' detection radius (and surrounded by the borders of multiple sensors),

    Therefore we can narrow down the coordinate candidates to the borders of known sensors, by calculating +1 to each sensor's coverage radius.

    This method bruteforce's all of these possible coordinates, stopping as soon as we find a node that is outside the range of all sensors.

    Return the Tuning Frequency.
    """

    # Create a tunnel map object with sensors and beacons populated
    tm = Tunnel_Map(input_data)

    # Calculate all the coordinates to check
    test_locations = queue.Queue()

    # Calculate all the edges of the sensors
    for sensor in tm.sensors:
        # Any hidden sensor is 1 cell outside the range of each sensor
        max_dx = sensor.manhattan_distance + 1

        for dx in range(max_dx + 1):
            test_col_l = sensor.location["col"] - dx
            test_col_r = sensor.location["col"] + dx
            test_row_hi = (
                sensor.location["row"] + sensor.manhattan_distance - dx + 1
            )
            test_row_lo = (
                sensor.location["row"] - sensor.manhattan_distance + dx - 1
            )

            # At the "points" of the diamond shape sensor range we will get duplicates.
            # Use a set to remove duplicates.
            test_cols = list(set([test_col_l, test_col_r]))
            test_rows = list(set([test_row_hi, test_row_lo]))

            # Generate the coordinates that will be tested around each sensor
            # Store all test coordinates in the queue
            for col in test_cols:
                for row in test_rows:
                    location = {"row": row, "col": col}
                    test_locations.put(location)

    # Create a dict to hold the coordinates of the hidden location
    hidden_beacon_location = {"row": None, "col": None}

    # Track if we have found the hidden location so we can stop brute forcing.
    hidden_beacon_location_found = False

    # How many potential hidden locations will we need to test?
    qsize = test_locations.qsize()
    print("QSize", qsize)

    # Work through the list of potential coordinates checking whether they are hidden
    for i in range(qsize):
        cell = test_locations.get()

        # print("Testing cell:", cell)
        if cell["row"] < 0:
            continue
        if cell["col"] < 0:
            continue
        if cell["row"] > search_range_max:
            continue
        if cell["col"] > search_range_max:
            continue

        this_cell = "."

        this_cell = _check_cell(tm, this_cell, cell["row"], cell["col"])

        if this_cell == ".":
            hidden_beacon_location["row"] = cell["row"]
            hidden_beacon_location["col"] = cell["col"]
            hidden_beacon_location_found = True

        if hidden_beacon_location_found:
            break

    print("Hidden beacon location:", hidden_beacon_location)

    # Calculate and return the Tuning Frequency
    tuning_frequency = (
        4000000 * hidden_beacon_location["col"]
    ) + hidden_beacon_location["row"]
    return tuning_frequency


def main():
    print("NB: Run this program from the root directory of the repository.\n")

    # Day of the AOC
    day = 15
    data_dir = f"data/{day:02}/"
    # file_name = f'{data_dir}day_{day:02}_test.txt'
    file_name = f'{data_dir}day_{day:02}_real.txt'

    # Define the example data file and the puzzle input data
    INPUT_DATA_PATH = Path.cwd() / f"./{file_name}"

    if not INPUT_DATA_PATH.is_file():
        raise FileNotFoundError(
            f"Error: File does not exist: {INPUT_DATA_PATH}"
        )

    with INPUT_DATA_PATH.open(mode="r", encoding="utf-8") as in_file_data:
        input_str = in_file_data.read()

    # print(f"Day {day:02} - Part 1:", part1(input_str, 10))
    print(f"Day {day:02} - Part 1:", part1(input_str, 2000000))
    # print(f"Day {day:02} - Part 2:", part2(input_str, 20))
    print(f"Day {day:02} - Part 2:", part2(input_str, 4000000))


if __name__ == "__main__":
    main()
    # with Profile() as profile:
    #     main()
    #     (Stats(profile).strip_dirs().sort_stats(SortKey.TIME).print_stats())
