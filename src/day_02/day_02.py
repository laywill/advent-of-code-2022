#!/usr/bin/env python3.11
"""Solution to Day 2 of AoC 2022."""

from pathlib import Path

# Dict to decode the hands of the opponent
hands_op = {
    "A": "Rock",
    "B": "Paper",
    "C": "Scissors",
}

# Dict to equate the move I play to what I score
score_hand = {"Rock": 1, "Paper": 2, "Scissors": 3}

# Dict to equate the result of the round to what I score
score_outcome = {"Lose": 0, "Draw": 3, "Win": 6}


def process_input(input_str):
    """From input string, return a tuple of rounds, each round is a tuple of moves."""
    list_of_rounds = list(input_str.split("\n"))

    for idx, string in enumerate(list_of_rounds):
        hands = string.strip().split(" ")
        list_of_rounds[idx] = tuple([hands[0], hands[1]])
    return tuple(list_of_rounds)


def part1(rounds):
    """Return total score, decoding my moves as Rock Paper or Scissors.

    Find the result given the hand I must play.
    """
    hands_me = {
        "X": "Rock",
        "Y": "Paper",
        "Z": "Scissors",
    }

    total_score = 0
    for _, hands in enumerate(rounds):
        op = hands_op[hands[0]]
        me = hands_me[hands[1]]

        if me == "Rock":
            total_score += score_hand["Rock"]
            if op == "Rock":
                total_score += score_outcome["Draw"]
            elif op == "Paper":
                total_score += score_outcome["Lose"]
            elif op == "Scissors":
                total_score += score_outcome["Win"]
            else:
                raise KeyError(f"Error: Key not found in hands_op dict {op}")
        elif me == "Paper":
            total_score += score_hand["Paper"]
            if op == "Rock":
                total_score += score_outcome["Win"]
            elif op == "Paper":
                total_score += score_outcome["Draw"]
            elif op == "Scissors":
                total_score += score_outcome["Lose"]
            else:
                raise KeyError(f"Error: Key not found in hands_op dict {op}")
        elif me == "Scissors":
            total_score += score_hand["Scissors"]
            if op == "Rock":
                total_score += score_outcome["Lose"]
            elif op == "Paper":
                total_score += score_outcome["Win"]
            elif op == "Scissors":
                total_score += score_outcome["Draw"]
            else:
                raise KeyError(f"Error: Key not found in hands_op dict {op}")
        else:
            raise KeyError(f"Error: Key not found in hands_me dict {op}")

    return total_score


def part2(rounds):
    """Return total score, decoding my hands as match result.

    Find the hand I must play to achieve that result.
    """
    result = {
        "X": "Lose",
        "Y": "Draw",
        "Z": "Win",
    }

    total_score = 0

    for _, hands in enumerate(rounds):
        op = hands_op[hands[0]]
        outcome = result[hands[1]]

        total_score += score_outcome[outcome]

        if outcome == "Draw":
            # I must have the same hand as the op
            total_score += score_hand[op]
        elif outcome == "Win":
            if op == "Rock":
                total_score += score_hand["Paper"]
            elif op == "Paper":
                total_score += score_hand["Scissors"]
            elif op == "Scissors":
                total_score += score_hand["Rock"]
            else:
                raise KeyError(f"Error: Key not found in hands_op dict {op}")
        elif outcome == "Lose":
            if op == "Rock":
                total_score += score_hand["Scissors"]
            elif op == "Paper":
                total_score += score_hand["Rock"]
            elif op == "Scissors":
                total_score += score_hand["Paper"]
            else:
                raise KeyError(f"Error: Key not found in hands_op dict {op}")
        else:
            raise KeyError(f"Error: Key not found in hands_me dict {op}")

    return total_score


if __name__ == "__main__":
    print("NB: Run this program from the root directory of the repository.\n")

    # Day of the AOC
    day = 2
    data_dir = f"data/{day:02}/"
    # file_name = f'{data_dir}day_{day:02}_test.txt'
    file_name = f'{data_dir}day_{day:02}_real.txt'

    # Define the example data file and the puzzle input data
    INPUT_DATA_PATH = Path.cwd() / f"./{file_name}"

    if not INPUT_DATA_PATH.is_file():
        raise FileNotFoundError(
            f"Error: File does not exist: {INPUT_DATA_PATH}"
        )

    with INPUT_DATA_PATH.open(mode="r", encoding="utf-8") as in_file_data:
        input_str = in_file_data.read().strip()
    game_hands = process_input(input_str)

    print(f"Day {day:02} - Part 1:", part1(game_hands))
    print(f"Day {day:02} - Part 2:", part2(game_hands))
