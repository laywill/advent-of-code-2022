#!/usr/bin/env python3.11
"""Solution to Day 7 of AoC 2022."""

from functools import reduce
from pathlib import Path

import numpy as np


def process_input(input_string: str) -> np.ndarray:
    """Return a numpy array of ints."""
    input_as_list_of_str = input_string.strip().split("\n")
    board = np.array(
        [
            [int(char) for char in list(line.strip())]
            for line in input_as_list_of_str
        ]
    )
    return board


class Trees:
    """Object to hold the tree map."""

    def __init__(self, board: np.ndarray):
        """
        Initialise the object with variables that will be used later.

        tree_map:       Matrix of tree heights
        shape:          The shape of the tree_map.
        num_rows:       The number of rows in the tree_map.
        num_cols:       The number of cols in the tree_map.

        visible:        Matrix of zeros and ones. Zero, tree not visible; One, tree is visible.
        scenic_scores:  Scenic score for each tree.

        row:            Current row we are investigating.
        col:            Current column we are investigating.
        """
        self.tree_map = np.array(board)

        self.shape = list(self.tree_map.shape)
        self.num_rows = self.shape[0]
        self.num_cols = self.shape[1]

        self.visible = np.zeros(self.shape, dtype=int)
        self.scenic_scores = np.zeros(self.shape, dtype=int)

        self.row = 0
        self.col = 0

    def _taller_than(self, input_arr: np.ndarray, less_than_val: int) -> bool:
        """Return true if less_than_val is greater than any value in input_arr."""
        range_max = np.amax(input_arr)
        return range_max < less_than_val

    def _mark_visible(self, row_idx: int, col_idx: int):
        """Set the value at (row_idx, col_idx) of the visible array to one."""
        self.visible[row_idx][col_idx] = 1

    def _get_views(self) -> dict:
        """Return the views left, right, up and down from current location."""
        view_lt = self.tree_map[self.row, 0 : (self.col)]
        view_rt = self.tree_map[self.row, (self.col + 1) : (self.num_cols)]
        view_up = self.tree_map[0 : (self.row), self.col]
        view_dn = self.tree_map[(self.row + 1) : (self.num_rows), self.col]

        ret_views = {
            "left": view_lt,
            "right": view_rt,
            "top": view_up,
            "bottom": view_dn,
        }

        return ret_views

    def _do_a_search(self) -> None:
        """Search adjacent trees to see if they are visible."""
        # if an edge, then mark it as visible
        if (self.row == 0) or (self.row == self.num_rows - 1):
            self._mark_visible(self.row, self.col)
            return None
        if (self.col == 0) or (self.col == self.num_cols - 1):
            self._mark_visible(self.row, self.col)
            return None

        # Height of our current tree
        current_tree = self.tree_map[self.row][self.col]

        # Define our trees left, right, up and down
        views = self._get_views()

        # If all trees between current tree and the edge are shorter, a tree is visible
        # Check if all trees are lower on the left, right, top and bottom.
        if self._taller_than(views["left"], current_tree):
            self._mark_visible(self.row, self.col)
        elif self._taller_than(views["right"], current_tree):
            self._mark_visible(self.row, self.col)
        elif self._taller_than(views["top"], current_tree):
            self._mark_visible(self.row, self.col)
        elif self._taller_than(views["bottom"], current_tree):
            self._mark_visible(self.row, self.col)

    def find_all_visible(self):
        """Update self.visible with all trees visible from the edge of the map."""
        # Search across rows, top to bottom, left to right
        row_range = range(self.num_rows)
        col_range = range(self.num_cols)
        for self.row in row_range:
            for self.col in col_range:
                self._do_a_search()

    def _trees_in_view(self, tree_view, threehouse_height):
        """Return how many tress we can see in a given range of trees and a current height."""
        for idx, val in enumerate(tree_view):
            if self._taller_than(tree_view[0 : idx + 1], threehouse_height):
                # We are taller, so keep searching, we can see more trees!
                continue
            else:
                # We are the same height or shorter, we know how many trees we can see
                return idx + 1

        # We looked all the way to the edge of the map, so we can see all trees in the view
        return len(tree_view)

    def _viewing_distances(self):
        """Return a list of viewing distances from a potential treehouse at current row/col."""
        # If all trees between this and the edge are shorter, a tree is visible
        current_tree = self.tree_map[self.row][self.col]

        # Define our trees in view on the left, right, up and down
        views = self._get_views()

        # How many trees can we see in each direction store in a list
        trees_in_view = []

        # Count the number of visible trees on the left, right, top, and bottom
        view = np.flip(views["left"])
        trees_in_view.append(self._trees_in_view(view, current_tree))

        view = views["right"]
        trees_in_view.append(self._trees_in_view(view, current_tree))

        view = np.flip(views["top"])
        trees_in_view.append(self._trees_in_view(view, current_tree))

        view = views["bottom"]
        trees_in_view.append(self._trees_in_view(view, current_tree))

        # Return a list of viewing distances
        return trees_in_view

    def _multiply_list_elements(self, ip_list: list[int]) -> int:
        """Return the result of multiplying all elements in a list together."""
        for i, v in enumerate(ip_list):
            if v is None:
                ip_list[i] = 0

        return reduce(lambda x, y: x * y, ip_list)

    def find_best_treehouse(self):
        """Return scenic score of best treehouse.

        The best treehouse has the highest scenic score.
        A tree's scenic score is found by multiplying together its viewing distance in each of the four directions.
        """
        # Don't even bother searching the edge nodes as anything of the edge of the map is treated as zero
        row_range = range(1, self.num_rows - 1)
        col_range = range(1, self.num_cols - 1)
        for self.row in row_range:
            for self.col in col_range:
                # Get the viewing distances for this location
                distances = self._viewing_distances()

                # Calculate scenic score and store
                scenic_score = self._multiply_list_elements(distances)
                self.scenic_scores[self.row][self.col] = scenic_score


def part1(trees) -> int:
    """Given starting coordinate, find number of visible trees."""
    # Discover which trees are visible
    trees.find_all_visible()

    # Return the total number of visible trees
    return np.sum(trees.visible)


def part2(trees):
    """Return the highest scenic score of any tree."""
    # Calculate the scenic score of each potential treehouse
    trees.find_best_treehouse()

    # Return the greatest scenic score
    return np.amax(trees.scenic_scores)


if __name__ == "__main__":
    print("NB: Run this program from the root directory of the repository.\n")

    # Day of the AOC
    day = 8
    data_dir = f"data/{day:02}/"
    # file_name = f'{data_dir}day_{day:02}_test.txt'
    file_name = f'{data_dir}day_{day:02}_real.txt'

    # Define the example data file and the puzzle input data
    INPUT_DATA_PATH = Path.cwd() / f"./{file_name}"

    if not INPUT_DATA_PATH.is_file():
        raise FileNotFoundError(
            f"Error: File does not exist: {INPUT_DATA_PATH}"
        )

    with INPUT_DATA_PATH.open(mode="r", encoding="utf-8") as in_file_data:
        input_str = in_file_data.read().strip()

    parsed_data = process_input(input_str)

    # Generate a Graph of vertices and edges
    tree_map = Trees(parsed_data)

    print(f"Day {day:02} - Part 1:", part1(tree_map))
    print(f"Day {day:02} - Part 2:", part2(tree_map))
