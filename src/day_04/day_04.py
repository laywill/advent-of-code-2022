#!/usr/bin/env python3.11
"""Solution to Day 4 of AoC 2022."""

import re
from pathlib import Path


def process_input(input_str):
    """From input string, return a tuple of dicts, containing the left elf and right elf range min and max."""
    list_of_section_pairs = list(input_str.split("\n"))

    for idx, string in enumerate(list_of_section_pairs):
        # What we’ve done here is passed in a raw string that re helps interpret.
        # We pass in the pipe character | as an or statement.
        # re.split() then splits string any time a character matching the regex is met.
        # We use map to cast the resulting list of strings to integers.
        coords = list(map(int, re.split(r'-|,', string)))

        # Populate a dict with the section details to make the rest
        # of our code neater.
        section_details = {
            "Left": {"min": coords[0], "max": coords[1]},
            "Right": {"min": coords[2], "max": coords[3]},
        }

        list_of_section_pairs[idx] = section_details

    return tuple(list_of_section_pairs)


def range_subset_of_range(range1, range2):
    """Return True if range1 is a subset of range 2.

    This is very efficient and is 0(1) to process so scales well.
    """
    return set(range1).issubset(range2)


def range_overlaps_range(range1, range2):
    """Return True if range1 overlaps range 2.

    This is very efficient and is 0(1) to process so scales well.
    """
    return set(range1).intersection(range2)


def generate_ranges_from_section(sec):
    """Return a tuple of ranges, corresponding to the left and right sections passed in."""
    # Range generates up-to but NOT including the max value
    # Add 1 to get it to generate the correct length line
    range_left = range(sec["Left"]["min"], sec["Left"]["max"] + 1)
    range_right = range(sec["Right"]["min"], sec["Right"]["max"] + 1)
    return tuple([range_left, range_right])


def part1(sections):
    """Return the total number of section pairs that totally overlap."""
    count = 0
    for section in sections:
        range_l, range_r = generate_ranges_from_section(section)
        if range_subset_of_range(range_l, range_r):
            count += 1
        elif range_subset_of_range(range_r, range_l):
            count += 1
    return count


def part2(sections):
    """Return the total number of section pairs that intersect."""
    count = 0
    for section in sections:
        range_l, range_r = generate_ranges_from_section(section)
        if range_overlaps_range(range_l, range_r):
            count += 1
        elif range_overlaps_range(range_r, range_l):
            count += 1
    return count


if __name__ == "__main__":
    print("NB: Run this program from the root directory of the repository.\n")

    # Day of the AOC
    day = 4
    data_dir = f"data/{day:02}/"
    # file_name = f'{data_dir}day_{day:02}_test.txt'
    file_name = f'{data_dir}day_{day:02}_real.txt'

    # Define the example data file and the puzzle input data
    INPUT_DATA_PATH = Path.cwd() / f"./{file_name}"

    if not INPUT_DATA_PATH.is_file():
        raise FileNotFoundError(
            f"Error: File does not exist: {INPUT_DATA_PATH}"
        )

    with INPUT_DATA_PATH.open(mode="r", encoding="utf-8") as in_file_data:
        input_str = in_file_data.read().strip()
    section_assignments = process_input(input_str)

    print(f"Day {day:02} - Part 1:", part1(section_assignments))
    print(f"Day {day:02} - Part 2:", part2(section_assignments))
