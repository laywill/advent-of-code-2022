#!/usr/bin/env python3.11
"""Solution to Day 12 of AoC 2022."""

import math
import string
from collections import defaultdict
from pathlib import Path
from queue import PriorityQueue

import numpy as np


def process_input(input_string: str) -> np.ndarray:
    """Return a numpy array of ints."""
    input_as_list_of_str = input_string.strip().split("\n")
    board = np.array(
        [
            [str(char) for char in list(line.strip())]
            for line in input_as_list_of_str
        ]
    )
    return board


class Graph:
    """Object to hold graph of vertices and edges to traverse."""

    def __init__(self, board: np.ndarray):
        """
        Initialise the object with variables that will be used later.

        edges:     Matrix of cost of entering node (u,v) = self.edges[u,v] = weight of the edge.
        visited:   A set which contains the visited vertices.
        cost_dict: A dictionary, with vertices as keys, the cost to traverse there as values.
        """
        self.edges = np.array(board)
        self.visited = set()
        self.cost_dict = {}

        self.shape = list(self.edges.shape)
        self.num_rows = self.shape[0]
        self.num_cols = self.shape[1]

    def dijkstra(self, start_vertex: tuple):
        """
        Return dictionary of coords : cost pairs to traverse to a given location.

        Use Dijkstra's algorithm to cost to get from the starting vertex to
        any other vertex in the graph.
        Here, vetices are given as XY coordinates in a matrix.

        Start vertex specifies the starting point to use as a coordinate (0,0)
        Representing (row, column)
        """
        # Shape = (rows, cols)
        g_shape = list(self.edges.shape)
        for row in range(self.num_rows):
            for col in range(self.num_cols):
                self.cost_dict[(row, col)] = float('inf')
        self.cost_dict[start_vertex] = 0

        index_to_check_queue = PriorityQueue()
        index_to_check_queue.put((0, start_vertex))

        while not index_to_check_queue.empty():
            (_, current_vertex) = index_to_check_queue.get()
            self.visited.add(current_vertex)

            # We only compare up, down, left and right from current_vertex
            # Express as 4 coordinate pairs to use as modifiers
            adjacent = [(-1, 0), (1, 0), (0, -1), (0, 1)]
            for _, modifier in enumerate(adjacent):
                row = current_vertex[0] + modifier[0]
                col = current_vertex[1] + modifier[1]

                if row < 0 or row > g_shape[0] - 1:
                    # Row out of bounds, skip
                    continue
                if col < 0 or col > g_shape[1] - 1:
                    # Col out of bounds, skip
                    continue

                neighbour = (row, col)
                if neighbour == current_vertex:
                    # Somehow we have ended up trying to compare with outselves
                    # Raise an error
                    raise ValueError("neighbour == current_vertex")

                if self.edges[current_vertex] == "S":
                    # Start location is equivalent to an "a". Overload this so the step finding algorithm works
                    self.edges[current_vertex] = "a"
                if self.edges[neighbour] == "E":
                    # Start location is equivalent to an "a". Overload this so the step finding algorithm works
                    self.edges[neighbour] = "z"

                # We can only go up one step at a time.
                # Apply step finding algorithm to only let us go down, same level, or up one step
                if self.edges[neighbour] > chr(
                    ord(self.edges[current_vertex]) + 1
                ):
                    # Cannot go up a step this high
                    continue

                if neighbour not in self.visited:
                    # Cost in this djikstra algorithm is the distance travelled
                    old_cost = self.cost_dict[neighbour]
                    new_cost = self.cost_dict[current_vertex] + 1
                    if new_cost < old_cost:
                        index_to_check_queue.put((new_cost, neighbour))
                        self.cost_dict[neighbour] = new_cost

        return self.cost_dict


def part1(input_data) -> int:
    """Given starting coordinate, find shortest steps to end."""
    # Generate a Graph of vetices and edges
    hill_climb_graph = Graph(input_data)

    start_coord = np.where(hill_climb_graph.edges == "S")
    start_coord = tuple([int(start_coord[0]), int(start_coord[1])])
    end_coord = np.where(hill_climb_graph.edges == "E")
    end_coord = tuple([int(end_coord[0]), int(end_coord[1])])
    print("End Coordinate:", end_coord)

    # Run Dijkstra's algorithm to find routes to each vertex from given start
    costs = hill_climb_graph.dijkstra(start_coord)

    # Return the cost of traversing to the desired destination
    return costs[end_coord]


def part2(input_data) -> int:
    """Return shortest number of steps to end from any valid starting point."""

    # Generate a Graph of vetices and edges
    hill_climb_graph = Graph(input_data)

    #  Start by finding all the nodes that are a valid start.
    # Valid starts must be "S" or "a"
    # Seems a bit easy if "S" is the shortest start so only search for "a"
    # Create an array of (row,col) coords to use
    valid_Starts = np.where(hill_climb_graph.edges == "a")
    valid_Starts = np.array((valid_Starts[0], valid_Starts[1]))
    valid_Starts = np.array(valid_Starts).T

    # End coordinate will be the same each time
    end_coord = np.where(hill_climb_graph.edges == "E")
    end_coord = tuple([int(end_coord[0]), int(end_coord[1])])

    # Need somewhere to store the cost of starting at each coordinate
    coords_cost = []

    for _, start_coord in enumerate(valid_Starts):
        # Have to cast the numpy array to a tuple of ints
        start_coord = tuple([int(start_coord[0]), int(start_coord[1])])

        disposable_hill_climb_graph = Graph(input_data)

        # Run Dijkstra's algorithm to find routes to each vertex from given start
        costs = disposable_hill_climb_graph.dijkstra(start_coord)

        coords_cost.append(costs[end_coord])

    # Return the cost of traversing to the desired des
    return min(coords_cost)


if __name__ == "__main__":
    print("NB: Run this program from the root directory of the repository.\n")

    # Day of the AOC
    day = 12
    data_dir = f"data/{day:02}/"
    # file_name = f'{data_dir}day_{day:02}_test_x_climb.txt'
    # file_name = f'{data_dir}day_{day:02}_test_y_climb.txt'
    # file_name = f'{data_dir}day_{day:02}_test_x_climb_with_drop.txt'
    # file_name = f'{data_dir}day_{day:02}_test_y_climb_with_drop.txt'
    # file_name = f'{data_dir}day_{day:02}_test_spiral.txt'
    # file_name = f'{data_dir}day_{day:02}_test.txt'
    file_name = f'{data_dir}day_{day:02}_real.txt'

    # Define the example data file and the puzzle input data
    INPUT_DATA_PATH = Path.cwd() / f"./{file_name}"

    if not INPUT_DATA_PATH.is_file():
        raise FileNotFoundError(
            f"Error: File does not exist: {INPUT_DATA_PATH}"
        )

    with INPUT_DATA_PATH.open(mode="r", encoding="utf-8") as in_file_data:
        input_str = in_file_data.read()

    parsed_data = process_input(input_str)

    print(f"Day {day:02} - Part 1:", part1(parsed_data))
    print(f"Day {day:02} - Part 2:", part2(parsed_data))
